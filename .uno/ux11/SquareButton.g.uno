[Uno.Compiler.UxGenerated]
public partial class SquareButton: Fuse.Controls.Button
{
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly SquareButton __parent;
        [Uno.WeakReference] internal readonly SquareButton __parentInstance;
        public Template(SquareButton parent, SquareButton parentInstance): base("GraphicsAppearance", false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "GraphicsAppearance"
        };
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Rectangle();
            var temp1 = new global::Fuse.Reactive.This();
            var temp = new global::StyledLabel();
            temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp2 = new global::Fuse.Reactive.Property(temp1, getAFuse_accessor_Fuse_Controls_ButtonBase_Text.Singleton);
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Read);
            var temp4 = new global::Fuse.Drawing.Stroke();
            var temp5 = new global::Fuse.Drawing.SolidColor();
            __self.Name = __selector1;
            temp.FontSize = 15f;
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Font = global::RotatingPages.HindBold;
            temp.Bindings.Add(temp3);
            temp4.Width = 2f;
            temp4.Brush = temp5;
            temp5.Color = float4(0.3333333f, 0.4313726f, 0.4784314f, 1f);
            __g_nametable.Objects.Add(__self);
            __self.Strokes.Add(temp4);
            __self.Children.Add(temp);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "GraphicsAppearance";
    }
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
    };
    static SquareButton()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SquareButton()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Drawing.SolidColor();
        var GraphicsAppearance = new Template(this, this);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        this.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(30f, 5f, 30f, 25f);
        temp.Color = float4(1f, 1f, 1f, 1f);
        __g_nametable.This = this;
        this.Background = temp;
        this.Templates.Add(GraphicsAppearance);
    }
}
