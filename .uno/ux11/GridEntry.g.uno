[Uno.Compiler.UxGenerated]
public partial class GridEntry: Fuse.Controls.Panel
{
    static GridEntry()
    {
    }
    [global::Uno.UX.UXConstructor]
    public GridEntry()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Rectangle();
        var temp1 = new global::Fuse.Drawing.Stroke();
        var temp2 = new global::Fuse.Drawing.SolidColor();
        this.Height = new Uno.UX.Size(45f, Uno.UX.Unit.Unspecified);
        temp.Strokes.Add(temp1);
        temp1.Width = 2f;
        temp1.Brush = temp2;
        temp2.Color = float4(0.8705882f, 0.9215686f, 0.9568627f, 1f);
        this.Children.Add(temp);
    }
}
