[Uno.Compiler.UxGenerated]
public partial class MyButton: Fuse.Controls.Rectangle
{
    string _field_Text;
    [global::Uno.UX.UXOriginSetter("SetText")]
    public string Text
    {
        get { return _field_Text; }
        set { SetText(value, null); }
    }
    public void SetText(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Text)
        {
            _field_Text = value;
            OnPropertyChanged("Text", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float> shadow_Size_inst;
    global::Uno.UX.Property<float> this_Opacity_inst;
    global::Uno.UX.Property<string> this_Text_inst;
    internal global::Fuse.Controls.Shadow shadow;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "shadow"
    };
    static MyButton()
    {
    }
    [global::Uno.UX.UXConstructor]
    public MyButton()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.This();
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, getAFuse_accessor_MyButton_Text.Singleton);
        this_Text_inst = new getAFuse_MyButton_Text_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        shadow = new global::Fuse.Controls.Shadow();
        shadow_Size_inst = new getAFuse_FuseControlsShadow_Size_Property(shadow, __selector2);
        this_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(this, __selector3);
        var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Read);
        var temp4 = new global::Fuse.Gestures.WhilePressed();
        var temp5 = new global::Fuse.Animations.Change<float>(shadow_Size_inst);
        var temp6 = new global::Fuse.Animations.Change<float>(this_Opacity_inst);
        this.CornerRadius = float4(3f, 3f, 3f, 3f);
        this.Color = float4(0f, 0.682353f, 0.9372549f, 1f);
        this.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        temp.Color = float4(1f, 1f, 1f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Margin = float4(30f, 15f, 30f, 15f);
        temp.Bindings.Add(temp3);
        shadow.Angle = 90f;
        shadow.Distance = 0f;
        shadow.Size = 0f;
        shadow.Color = float4(0f, 0f, 0f, 0.5333334f);
        shadow.Name = __selector4;
        temp4.Animators.Add(temp5);
        temp4.Animators.Add(temp6);
        temp5.Value = 10f;
        temp5.Duration = 0.05;
        temp5.DurationBack = 0.2;
        temp6.Value = 0.8f;
        temp6.Duration = 0.05;
        temp6.DurationBack = 0.2;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(shadow);
        __g_nametable.Properties.Add(this_Text_inst);
        this.Children.Add(temp);
        this.Children.Add(shadow);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Text";
    static global::Uno.UX.Selector __selector2 = "Size";
    static global::Uno.UX.Selector __selector3 = "Opacity";
    static global::Uno.UX.Selector __selector4 = "shadow";
}
