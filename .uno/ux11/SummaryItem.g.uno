[Uno.Compiler.UxGenerated]
public partial class SummaryItem: Fuse.Controls.Panel
{
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly SummaryItem __parent;
        [Uno.WeakReference] internal readonly SummaryItem __parentInstance;
        public Template(SummaryItem parent, SummaryItem parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Url_inst;
        global::Uno.UX.Property<string> temp1_Value_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb17;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "temp_eb17"
        };
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.StackPanel();
            var temp = new global::Fuse.Controls.Image();
            temp_Url_inst = new getAFuse_FuseControlsImage_Url_Property(temp, __selector0);
            var temp2 = new global::Fuse.Reactive.Data("cover_file");
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp1 = new global::Fuse.Controls.Text();
            temp1_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp1, __selector1);
            var temp3 = new global::Fuse.Reactive.Data("title");
            var temp4 = new global::Fuse.Reactive.Data("openDetail");
            var temp5 = new global::Fuse.Reactive.DataBinding(temp_Url_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp6 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp3, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp7 = new global::Fuse.Gestures.Clicked();
            var temp8 = new global::Fuse.Triggers.Actions.Callback();
            var temp_eb17 = new global::Fuse.Reactive.EventBinding(temp4, __g_nametable);
            __self.Width = new Uno.UX.Size(133f, Uno.UX.Unit.Unspecified);
            __self.Margin = float4(0f, 0f, 0f, 0f);
            temp.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp.Height = new Uno.UX.Size(199f, Uno.UX.Unit.Unspecified);
            temp.Margin = float4(10f, 0f, 10f, 0f);
            temp.Bindings.Add(temp5);
            temp1.MaxLength = 18;
            temp1.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
            temp1.FontSize = 12f;
            temp1.TextAlignment = Fuse.Controls.TextAlignment.Center;
            temp1.Bindings.Add(temp6);
            temp7.Actions.Add(temp8);
            temp7.Bindings.Add(temp_eb17);
            temp8.Handler += temp_eb17.OnEvent;
            __g_nametable.Objects.Add(temp_eb17);
            __self.Children.Add(temp);
            __self.Children.Add(temp1);
            __self.Children.Add(temp7);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Url";
        static global::Uno.UX.Selector __selector1 = "Value";
    }
    global::Uno.UX.Property<object> temp_Items_inst;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
    };
    static SummaryItem()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SummaryItem()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp, __selector0);
        var temp1 = new global::Fuse.Reactive.Data("summary");
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp2 = new global::Fuse.Controls.ScrollView();
        var temp3 = new global::Fuse.Controls.StackPanel();
        var temp4 = new Template(this, this);
        var temp5 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
        this.Margin = float4(0f, 5f, 0f, 5f);
        temp2.AllowedScrollDirections = Fuse.Controls.ScrollDirections.Horizontal;
        temp2.Children.Add(temp3);
        temp3.Orientation = Fuse.Layouts.Orientation.Horizontal;
        temp3.Children.Add(temp);
        temp.Templates.Add(temp4);
        temp.Bindings.Add(temp5);
        __g_nametable.This = this;
        this.Children.Add(temp2);
    }
    static global::Uno.UX.Selector __selector0 = "Items";
}
