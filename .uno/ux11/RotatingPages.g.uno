[Uno.Compiler.UxGenerated]
public partial class RotatingPages: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly RotatingPages __parent;
        [Uno.WeakReference] internal readonly RotatingPages __parentInstance;
        public Template(RotatingPages parent, RotatingPages parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        [Uno.Compiler.UxGenerated]
        public partial class Template1: Uno.UX.Template
        {
            [Uno.WeakReference] internal readonly Template __parent;
            [Uno.WeakReference] internal readonly Fuse.Controls.Panel __parentInstance;
            public Template1(Template parent, Fuse.Controls.Panel parentInstance): base(null, false)
            {
                __parent = parent;
                __parentInstance = parentInstance;
            }
            global::Uno.UX.Property<string> __self_Text_inst;
            global::Uno.UX.NameTable __g_nametable;
            static string[] __g_static_nametable = new string[] {
            };
            static Template1()
            {
            }
            public override object New()
            {
                var __self = new global::BulletPointLabel();
                __self_Text_inst = new getAFuse_BulletPointLabel_Text_Property(__self, __selector0);
                var temp = new global::Fuse.Reactive.Data("");
                __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
                var temp1 = new global::Fuse.Reactive.DataBinding(__self_Text_inst, temp, __g_nametable, Fuse.Reactive.BindingMode.Default);
                __self.Bindings.Add(temp1);
                return __self;
            }
            static global::Uno.UX.Selector __selector0 = "Text";
        }
        [Uno.Compiler.UxGenerated]
        public partial class Template2: Uno.UX.Template
        {
            [Uno.WeakReference] internal readonly Template __parent;
            [Uno.WeakReference] internal readonly Fuse.Controls.Panel __parentInstance;
            public Template2(Template parent, Fuse.Controls.Panel parentInstance): base("Dot", false)
            {
                __parent = parent;
                __parentInstance = parentInstance;
            }
            global::Uno.UX.Property<float> interiorCircle_Opacity_inst;
            internal global::Fuse.Controls.Circle interiorCircle;
            static Template2()
            {
            }
            public override object New()
            {
                var __self = new global::Fuse.Controls.Panel();
                var interiorCircle = new global::Fuse.Controls.Circle();
                interiorCircle_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(interiorCircle, __selector0);
                var temp = new global::Fuse.Controls.Circle();
                var temp1 = new global::Fuse.Drawing.Stroke();
                var temp2 = new global::Fuse.Navigation.ActivatingAnimation();
                var temp3 = new global::Fuse.Animations.Change<float>(interiorCircle_Opacity_inst);
                __self.Width = new Uno.UX.Size(15f, Uno.UX.Unit.Unspecified);
                __self.Height = new Uno.UX.Size(15f, Uno.UX.Unit.Unspecified);
                __self.Margin = float4(5f, 5f, 5f, 5f);
                __self.Name = __selector1;
                temp.Strokes.Add(temp1);
                temp.Children.Add(interiorCircle);
                temp.Children.Add(temp2);
                temp1.Color = float4(0.8705882f, 0.9215686f, 0.9568627f, 1f);
                temp1.Width = 2f;
                interiorCircle.Color = float4(0.8705882f, 0.9215686f, 0.9568627f, 1f);
                interiorCircle.Opacity = 0f;
                interiorCircle.Name = __selector2;
                temp2.Animators.Add(temp3);
                temp3.Value = 1f;
                __self.Children.Add(temp);
                return __self;
            }
            static global::Uno.UX.Selector __selector0 = "Opacity";
            static global::Uno.UX.Selector __selector1 = "Dot";
            static global::Uno.UX.Selector __selector2 = "interiorCircle";
        }
        [Uno.Compiler.UxGenerated]
        public partial class Template3: Uno.UX.Template
        {
            [Uno.WeakReference] internal readonly Template __parent;
            [Uno.WeakReference] internal readonly Fuse.Controls.Panel __parentInstance;
            public Template3(Template parent, Fuse.Controls.Panel parentInstance): base(null, false)
            {
                __parent = parent;
                __parentInstance = parentInstance;
            }
            global::Uno.UX.Property<Fuse.Elements.HitTestMode> __self_HitTestMode_inst;
            global::Uno.UX.Property<float> __self_Opacity_inst;
            global::Uno.UX.Property<Uno.UX.FileSource> temp_File_inst;
            global::Uno.UX.NameTable __g_nametable;
            static string[] __g_static_nametable = new string[] {
                "imageContainer"
            };
            static Template3()
            {
            }
            public override object New()
            {
                var __self = new global::Fuse.Controls.Panel();
                __self_HitTestMode_inst = new getAFuse_FuseElementsElement_HitTestMode_Property(__self, __selector0);
                __self_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(__self, __selector1);
                var temp = new global::Fuse.Controls.Image();
                temp_File_inst = new getAFuse_FuseControlsImage_File_Property(temp, __selector2);
                var temp1 = new global::Fuse.Reactive.Data("");
                __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
                var temp2 = new global::Fuse.Navigation.WhileInactive();
                var temp3 = new global::Fuse.Animations.Change<Fuse.Elements.HitTestMode>(__self_HitTestMode_inst);
                var temp4 = new global::Fuse.Navigation.EnteringAnimation();
                var temp5 = new global::Fuse.Animations.Move();
                var temp6 = new global::Fuse.Animations.Change<float>(__self_Opacity_inst);
                var temp7 = new global::Fuse.Navigation.ExitingAnimation();
                var temp8 = new global::Fuse.Animations.Scale();
                var temp9 = new global::Fuse.Animations.Change<float>(__self_Opacity_inst);
                var temp10 = new global::Fuse.Reactive.DataBinding(temp_File_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
                __self.Name = __selector3;
                temp2.Animators.Add(temp3);
                temp3.Value = Fuse.Elements.HitTestMode.None;
                temp4.Animators.Add(temp5);
                temp4.Animators.Add(temp6);
                temp5.X = -1f;
                temp5.Duration = 1;
                temp5.RelativeTo = Fuse.TranslationModes.ParentSize;
                temp6.Value = 0f;
                temp6.Duration = 1;
                temp6.Easing = Fuse.Animations.Easing.ExponentialOut;
                temp7.Animators.Add(temp8);
                temp7.Animators.Add(temp9);
                temp8.Factor = 0.5f;
                temp8.Duration = 1;
                temp8.Target = __self;
                temp9.Value = 0f;
                temp9.Duration = 0.9;
                temp9.Easing = Fuse.Animations.Easing.ExponentialIn;
                temp.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
                temp.ContentAlignment = Fuse.Elements.Alignment.Top;
                temp.Bindings.Add(temp10);
                __g_nametable.Objects.Add(__self);
                __self.Children.Add(temp2);
                __self.Children.Add(temp4);
                __self.Children.Add(temp7);
                __self.Children.Add(temp);
                return __self;
            }
            static global::Uno.UX.Selector __selector0 = "HitTestMode";
            static global::Uno.UX.Selector __selector1 = "Opacity";
            static global::Uno.UX.Selector __selector2 = "File";
            static global::Uno.UX.Selector __selector3 = "imageContainer";
        }
        global::Uno.UX.Property<float> bottomPart_Opacity_inst;
        global::Uno.UX.Property<float> topScaling_Factor_inst;
        global::Uno.UX.Property<float> topPart_Opacity_inst;
        global::Uno.UX.Property<float> pageIndicatorPart_Opacity_inst;
        global::Uno.UX.Property<Fuse.Elements.HitTestMode> __self_HitTestMode_inst;
        global::Uno.UX.Property<float> pageTranslation_Y_inst;
        global::Uno.UX.Property<float> moreDiv_Opacity_inst;
        global::Uno.UX.Property<float> infoPointPanel_Opacity_inst;
        global::Uno.UX.Property<float> ratingsGrid_Opacity_inst;
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<string> temp1_Value_inst;
        global::Uno.UX.Property<string> temp2_Value_inst;
        global::Uno.UX.Property<object> temp3_Items_inst;
        global::Uno.UX.Property<string> temp4_Value_inst;
        global::Uno.UX.Property<string> temp5_Value_inst;
        global::Uno.UX.Property<object> temp6_Items_inst;
        internal global::Fuse.Controls.Panel bottomPartOuter;
        internal global::Fuse.Controls.Panel bottomPart;
        internal global::Fuse.Translation pageTranslation;
        internal global::Divider moreDiv;
        internal global::Fuse.Controls.StackPanel infoPointPanel;
        internal global::Fuse.Controls.Grid ratingsGrid;
        internal global::Fuse.Controls.Panel pageIndicatorPart;
        internal global::Fuse.Controls.Panel topPart;
        internal global::Fuse.Scaling topScaling;
        internal global::Fuse.Controls.PageControl pictureNavigator;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "bottomPartOuter",
            "bottomPart",
            "pageTranslation",
            "moreDiv",
            "infoPointPanel",
            "ratingsGrid",
            "pageIndicatorPart",
            "topPart",
            "topScaling",
            "pictureNavigator",
            "container"
        };
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var bottomPart = new global::Fuse.Controls.Panel();
            bottomPart_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(bottomPart, __selector0);
            var topScaling = new global::Fuse.Scaling();
            topScaling_Factor_inst = new getAFuse_FuseScaling_Factor_Property(topScaling, __selector1);
            var topPart = new global::Fuse.Controls.Panel();
            topPart_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(topPart, __selector0);
            var pageIndicatorPart = new global::Fuse.Controls.Panel();
            pageIndicatorPart_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(pageIndicatorPart, __selector0);
            __self_HitTestMode_inst = new getAFuse_FuseElementsElement_HitTestMode_Property(__self, __selector2);
            var pageTranslation = new global::Fuse.Translation();
            pageTranslation_Y_inst = new getAFuse_FuseTranslation_Y_Property(pageTranslation, __selector3);
            var moreDiv = new global::Divider();
            moreDiv_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(moreDiv, __selector0);
            var infoPointPanel = new global::Fuse.Controls.StackPanel();
            infoPointPanel_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(infoPointPanel, __selector0);
            var ratingsGrid = new global::Fuse.Controls.Grid();
            ratingsGrid_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(ratingsGrid, __selector0);
            var temp = new global::StyledLabel();
            temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector4);
            var temp7 = new global::Fuse.Reactive.Data("name");
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp1 = new global::StyledLabel();
            temp1_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp1, __selector4);
            var temp8 = new global::Fuse.Reactive.Data("price");
            var temp2 = new global::StyledLabel();
            temp2_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp2, __selector4);
            var temp9 = new global::Fuse.Reactive.Data("description");
            var temp3 = new global::Fuse.Reactive.Each();
            temp3_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp3, __selector5);
            var temp10 = new global::Fuse.Reactive.Data("infoPoints");
            var temp4 = new global::StyledLabel();
            temp4_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp4, __selector4);
            var temp11 = new global::Fuse.Reactive.Data("favorites");
            var temp5 = new global::StyledLabel();
            temp5_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp5, __selector4);
            var temp12 = new global::Fuse.Reactive.Data("reviews");
            var pictureNavigator = new global::Fuse.Controls.PageControl();
            var temp6 = new global::Fuse.Reactive.Each();
            temp6_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp6, __selector5);
            var temp13 = new global::Fuse.Reactive.Data("images");
            var temp14 = new global::Fuse.Navigation.EnteringAnimation();
            var temp15 = new global::Fuse.Animations.Rotate();
            var temp16 = new global::Fuse.Animations.Change<float>(bottomPart_Opacity_inst);
            var temp17 = new global::Fuse.Animations.Change<float>(topScaling_Factor_inst);
            var temp18 = new global::Fuse.Animations.Change<float>(topPart_Opacity_inst);
            var temp19 = new global::Fuse.Animations.Change<float>(pageIndicatorPart_Opacity_inst);
            var temp20 = new global::Fuse.Navigation.ExitingAnimation();
            var temp21 = new global::Fuse.Animations.Rotate();
            var temp22 = new global::Fuse.Animations.Change<float>(bottomPart_Opacity_inst);
            var temp23 = new global::Fuse.Animations.Change<float>(topScaling_Factor_inst);
            var temp24 = new global::Fuse.Animations.Change<float>(topPart_Opacity_inst);
            var temp25 = new global::Fuse.Animations.Change<float>(pageIndicatorPart_Opacity_inst);
            var temp26 = new global::Fuse.Navigation.WhileInactive();
            var temp27 = new global::Fuse.Animations.Change<Fuse.Elements.HitTestMode>(__self_HitTestMode_inst);
            var bottomPartOuter = new global::Fuse.Controls.Panel();
            var temp28 = new global::Fuse.Controls.Panel();
            var temp29 = new global::SquareButton();
            var temp30 = new global::Fuse.Controls.Panel();
            var temp31 = new global::Fuse.Gestures.SwipingAnimation(__parent.swipeGesture);
            var temp32 = new global::Fuse.Animations.Change<float>(pageTranslation_Y_inst);
            var temp33 = new global::Fuse.Animations.Change<float>(moreDiv_Opacity_inst);
            var temp34 = new global::Fuse.Animations.Change<float>(infoPointPanel_Opacity_inst);
            var temp35 = new global::Fuse.Animations.Change<float>(ratingsGrid_Opacity_inst);
            var temp36 = new global::Fuse.Controls.StackPanel();
            var temp37 = new global::Fuse.Controls.StackPanel();
            var temp38 = new global::ColorCircle();
            var temp39 = new global::ColorCircle();
            var temp40 = new global::ColorCircle();
            var temp41 = new global::ColorCircleSelected();
            var temp42 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp7, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp43 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp8, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp44 = new global::Fuse.Controls.StackPanel();
            var temp45 = new global::SizeButton();
            var temp46 = new global::SizeButtonSelected();
            var temp47 = new global::SizeButton();
            var temp48 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp9, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp49 = new Template1(this, __self);
            var temp50 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp10, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp51 = new global::GridEntry();
            var temp52 = new global::Fuse.Reactive.DataBinding(temp4_Value_inst, temp11, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp53 = new global::GridEntry();
            var temp54 = new global::Fuse.Reactive.DataBinding(temp5_Value_inst, temp12, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp55 = new global::Fuse.Controls.Rectangle();
            var temp56 = new global::Fuse.Controls.Shadow();
            var temp57 = new global::Fuse.Controls.PageIndicator(pictureNavigator);
            var Dot = new Template2(this, __self);
            var temp58 = new global::Fuse.Gestures.SwipeGesture();
            var imageContainer = new Template3(this, __self);
            var temp59 = new global::Fuse.Reactive.DataBinding(temp6_Items_inst, temp13, __g_nametable, Fuse.Reactive.BindingMode.Default);
            __self.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
            __self.Margin = float4(10f, 10f, 10f, 0f);
            __self.Name = __selector6;
            temp14.Animators.Add(temp15);
            temp14.Animators.Add(temp16);
            temp14.Animators.Add(temp17);
            temp14.Animators.Add(temp18);
            temp14.Animators.Add(temp19);
            temp15.DegreesY = 70f;
            temp15.Duration = 1;
            temp15.Target = bottomPartOuter;
            temp16.Value = 0f;
            temp16.Duration = 0.75;
            temp16.Delay = 0;
            temp16.Easing = Fuse.Animations.Easing.ExponentialIn;
            temp17.Value = 0.5f;
            temp17.Duration = 1;
            temp18.Value = 0f;
            temp18.Duration = 1;
            temp18.Easing = Fuse.Animations.Easing.ExponentialOut;
            temp19.Value = 0f;
            temp19.Duration = 1;
            temp19.Easing = Fuse.Animations.Easing.ExponentialOut;
            temp20.Animators.Add(temp21);
            temp20.Animators.Add(temp22);
            temp20.Animators.Add(temp23);
            temp20.Animators.Add(temp24);
            temp20.Animators.Add(temp25);
            temp21.DegreesY = -70f;
            temp21.Duration = 1;
            temp21.Target = bottomPartOuter;
            temp22.Value = 0f;
            temp22.Duration = 0.75;
            temp22.Delay = 0;
            temp22.Easing = Fuse.Animations.Easing.ExponentialIn;
            temp23.Value = 0.5f;
            temp23.Duration = 1;
            temp24.Value = 0f;
            temp24.Duration = 1;
            temp24.Easing = Fuse.Animations.Easing.ExponentialOut;
            temp25.Value = 0f;
            temp25.Duration = 1;
            temp25.Easing = Fuse.Animations.Easing.ExponentialOut;
            temp26.Animators.Add(temp27);
            temp27.Value = Fuse.Elements.HitTestMode.None;
            bottomPartOuter.Name = __selector7;
            bottomPartOuter.TransformOrigin = Fuse.Elements.TransformOrigins.HorizontalBoxCenter;
            bottomPartOuter.Children.Add(bottomPart);
            bottomPart.Name = __selector8;
            bottomPart.Children.Add(temp28);
            bottomPart.Children.Add(temp30);
            temp28.Alignment = Fuse.Elements.Alignment.Bottom;
            temp28.Children.Add(temp29);
            temp29.Text = "ADD TO CART";
            temp30.Children.Add(temp31);
            temp30.Children.Add(pageTranslation);
            temp30.Children.Add(temp36);
            temp30.Children.Add(temp55);
            temp31.Animators.Add(temp32);
            temp31.Animators.Add(temp33);
            temp31.Animators.Add(temp34);
            temp31.Animators.Add(temp35);
            temp32.Value = 0.12f;
            temp33.Value = 1f;
            temp33.Easing = Fuse.Animations.Easing.ExponentialOut;
            temp34.Value = 1f;
            temp34.Easing = Fuse.Animations.Easing.CubicIn;
            temp35.Value = 1f;
            temp35.Easing = Fuse.Animations.Easing.ExponentialIn;
            pageTranslation.Y = 0.46f;
            pageTranslation.Name = __selector9;
            pageTranslation.RelativeTo = Fuse.TranslationModes.ParentSize;
            temp36.ItemSpacing = 5f;
            temp36.Children.Add(temp37);
            temp36.Children.Add(temp);
            temp36.Children.Add(temp1);
            temp36.Children.Add(temp44);
            temp36.Children.Add(temp2);
            temp36.Children.Add(moreDiv);
            temp36.Children.Add(infoPointPanel);
            temp36.Children.Add(ratingsGrid);
            temp37.Orientation = Fuse.Layouts.Orientation.Horizontal;
            temp37.Alignment = Fuse.Elements.Alignment.Center;
            temp37.Children.Add(temp38);
            temp37.Children.Add(temp39);
            temp37.Children.Add(temp40);
            temp37.Children.Add(temp41);
            temp38.Color = float4(0.9490196f, 0.3215686f, 0.3294118f, 1f);
            temp39.Color = float4(0.3882353f, 0.5372549f, 0.6862745f, 1f);
            temp40.Color = float4(0.4392157f, 0.7568628f, 0.7019608f, 1f);
            temp41.Color = float4(1f, 0.9019608f, 0.3921569f, 1f);
            temp.FontSize = 23f;
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Font = global::RotatingPages.HindBold;
            temp.Bindings.Add(temp42);
            temp1.FontSize = 20f;
            temp1.Alignment = Fuse.Elements.Alignment.Center;
            temp1.Bindings.Add(temp43);
            temp44.Orientation = Fuse.Layouts.Orientation.Horizontal;
            temp44.Alignment = Fuse.Elements.Alignment.Center;
            temp44.Children.Add(temp45);
            temp44.Children.Add(temp46);
            temp44.Children.Add(temp47);
            temp45.Label = "S";
            temp46.Label = "M";
            temp47.Label = "L";
            temp2.Alignment = Fuse.Elements.Alignment.Center;
            temp2.Margin = float4(10f, 0f, 10f, 0f);
            temp2.Bindings.Add(temp48);
            moreDiv.Opacity = 0f;
            moreDiv.Name = __selector10;
            infoPointPanel.Alignment = Fuse.Elements.Alignment.Center;
            infoPointPanel.Opacity = 0f;
            infoPointPanel.Name = __selector11;
            infoPointPanel.Children.Add(temp3);
            temp3.Templates.Add(temp49);
            temp3.Bindings.Add(temp50);
            ratingsGrid.ColumnCount = 2;
            ratingsGrid.Margin = float4(30f, 20f, 30f, 20f);
            ratingsGrid.Opacity = 0f;
            ratingsGrid.Name = __selector12;
            ratingsGrid.Children.Add(temp51);
            ratingsGrid.Children.Add(temp53);
            temp51.Children.Add(temp4);
            temp4.Alignment = Fuse.Elements.Alignment.Center;
            temp4.Bindings.Add(temp52);
            temp53.Children.Add(temp5);
            temp5.Alignment = Fuse.Elements.Alignment.Center;
            temp5.Bindings.Add(temp54);
            temp55.CornerRadius = float4(8f, 8f, 8f, 8f);
            temp55.Color = float4(1f, 1f, 1f, 1f);
            temp55.Layer = Fuse.Layer.Background;
            temp55.Children.Add(temp56);
            temp56.Angle = 90f;
            temp56.Size = 4f;
            pageIndicatorPart.Height = new Uno.UX.Size(46f, Uno.UX.Unit.Percent);
            pageIndicatorPart.Alignment = Fuse.Elements.Alignment.Top;
            pageIndicatorPart.Name = __selector13;
            pageIndicatorPart.Children.Add(temp57);
            temp57.Alignment = Fuse.Elements.Alignment.BottomCenter;
            temp57.Templates.Add(Dot);
            topPart.Height = new Uno.UX.Size(60f, Uno.UX.Unit.Percent);
            topPart.Alignment = Fuse.Elements.Alignment.Top;
            topPart.Name = __selector14;
            topPart.Children.Add(temp58);
            topPart.Children.Add(topScaling);
            topPart.Children.Add(pictureNavigator);
            temp58.Direction = Fuse.Gestures.SwipeDirection.Up;
            topScaling.Factor = 1f;
            topScaling.Name = __selector15;
            pictureNavigator.Transition = Fuse.Controls.NavigationControlTransition.None;
            pictureNavigator.Name = __selector16;
            pictureNavigator.Children.Add(temp6);
            temp6.Templates.Add(imageContainer);
            temp6.Bindings.Add(temp59);
            __g_nametable.Objects.Add(bottomPartOuter);
            __g_nametable.Objects.Add(bottomPart);
            __g_nametable.Objects.Add(pageTranslation);
            __g_nametable.Objects.Add(moreDiv);
            __g_nametable.Objects.Add(infoPointPanel);
            __g_nametable.Objects.Add(ratingsGrid);
            __g_nametable.Objects.Add(pageIndicatorPart);
            __g_nametable.Objects.Add(topPart);
            __g_nametable.Objects.Add(topScaling);
            __g_nametable.Objects.Add(pictureNavigator);
            __g_nametable.Objects.Add(__self);
            __self.Children.Add(temp14);
            __self.Children.Add(temp20);
            __self.Children.Add(temp26);
            __self.Children.Add(bottomPartOuter);
            __self.Children.Add(pageIndicatorPart);
            __self.Children.Add(topPart);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Opacity";
        static global::Uno.UX.Selector __selector1 = "Factor";
        static global::Uno.UX.Selector __selector2 = "HitTestMode";
        static global::Uno.UX.Selector __selector3 = "Y";
        static global::Uno.UX.Selector __selector4 = "Value";
        static global::Uno.UX.Selector __selector5 = "Items";
        static global::Uno.UX.Selector __selector6 = "container";
        static global::Uno.UX.Selector __selector7 = "bottomPartOuter";
        static global::Uno.UX.Selector __selector8 = "bottomPart";
        static global::Uno.UX.Selector __selector9 = "pageTranslation";
        static global::Uno.UX.Selector __selector10 = "moreDiv";
        static global::Uno.UX.Selector __selector11 = "infoPointPanel";
        static global::Uno.UX.Selector __selector12 = "ratingsGrid";
        static global::Uno.UX.Selector __selector13 = "pageIndicatorPart";
        static global::Uno.UX.Selector __selector14 = "topPart";
        static global::Uno.UX.Selector __selector15 = "topScaling";
        static global::Uno.UX.Selector __selector16 = "pictureNavigator";
    }
    global::Uno.UX.Property<float> menuBtn_Opacity_inst;
    global::Uno.UX.Property<float> closeBtn_Opacity_inst;
    global::Uno.UX.Property<object> temp_Items_inst;
    [global::Uno.UX.UXGlobalResource("FontColor")] public static readonly Uno.Float4 FontColor;
    [global::Uno.UX.UXGlobalResource("LighterFontColor")] public static readonly Uno.Float4 LighterFontColor;
    [global::Uno.UX.UXGlobalResource("InactiveColor")] public static readonly Uno.Float4 InactiveColor;
    [global::Uno.UX.UXGlobalResource("HintColor")] public static readonly Uno.Float4 HintColor;
    [global::Uno.UX.UXGlobalResource("CardBackground")] public static readonly Uno.Float4 CardBackground;
    [global::Uno.UX.UXGlobalResource("BackgroundColor")] public static readonly Uno.Float4 BackgroundColor;
    [global::Uno.UX.UXGlobalResource("ColorSelRed")] public static readonly Uno.Float4 ColorSelRed;
    [global::Uno.UX.UXGlobalResource("ColorSelBlue")] public static readonly Uno.Float4 ColorSelBlue;
    [global::Uno.UX.UXGlobalResource("ColorSelGreen")] public static readonly Uno.Float4 ColorSelGreen;
    [global::Uno.UX.UXGlobalResource("ColorSelYellow")] public static readonly Uno.Float4 ColorSelYellow;
    [global::Uno.UX.UXGlobalResource("HindBold")] public static readonly Fuse.Font HindBold;
    [global::Uno.UX.UXGlobalResource("Hind")] public static readonly Fuse.Font Hind;
    internal global::Fuse.Gestures.SwipeGesture swipeGesture;
    internal global::Fuse.Controls.Panel closeBtn;
    internal global::Fuse.Controls.Panel menuBtn;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "swipeGesture",
        "closeBtn",
        "menuBtn"
    };
    static RotatingPages()
    {
        FontColor = float4(0.3333333f, 0.4313726f, 0.4784314f, 1f);
        LighterFontColor = float4(0.3333333f, 0.427451f, 0.4745098f, 1f);
        InactiveColor = float4(0.6705883f, 0.7098039f, 0.7372549f, 1f);
        HintColor = float4(0.8705882f, 0.9215686f, 0.9568627f, 1f);
        CardBackground = float4(1f, 1f, 1f, 1f);
        BackgroundColor = float4(0.9529412f, 0.9529412f, 0.9607843f, 1f);
        ColorSelRed = float4(0.9490196f, 0.3215686f, 0.3294118f, 1f);
        ColorSelBlue = float4(0.3882353f, 0.5372549f, 0.6862745f, 1f);
        ColorSelGreen = float4(0.4392157f, 0.7568628f, 0.7019608f, 1f);
        ColorSelYellow = float4(1f, 0.9019608f, 0.3921569f, 1f);
        HindBold = new global::Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/Hind-Bold.otf")));
        Hind = new global::Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/Hind-Regular.otf")));
        global::Uno.UX.Resource.SetGlobalKey(FontColor, "FontColor");
        global::Uno.UX.Resource.SetGlobalKey(LighterFontColor, "LighterFontColor");
        global::Uno.UX.Resource.SetGlobalKey(InactiveColor, "InactiveColor");
        global::Uno.UX.Resource.SetGlobalKey(HintColor, "HintColor");
        global::Uno.UX.Resource.SetGlobalKey(CardBackground, "CardBackground");
        global::Uno.UX.Resource.SetGlobalKey(BackgroundColor, "BackgroundColor");
        global::Uno.UX.Resource.SetGlobalKey(ColorSelRed, "ColorSelRed");
        global::Uno.UX.Resource.SetGlobalKey(ColorSelBlue, "ColorSelBlue");
        global::Uno.UX.Resource.SetGlobalKey(ColorSelGreen, "ColorSelGreen");
        global::Uno.UX.Resource.SetGlobalKey(ColorSelYellow, "ColorSelYellow");
        global::Uno.UX.Resource.SetGlobalKey(HindBold, "HindBold");
        global::Uno.UX.Resource.SetGlobalKey(Hind, "Hind");
    }
    [global::Uno.UX.UXConstructor]
    public RotatingPages(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        swipeGesture = new global::Fuse.Gestures.SwipeGesture();
        menuBtn = new global::Fuse.Controls.Panel();
        menuBtn_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(menuBtn, __selector0);
        closeBtn = new global::Fuse.Controls.Panel();
        closeBtn_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(closeBtn, __selector0);
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp, __selector1);
        var temp1 = new global::Fuse.Reactive.Data("items");
        var temp2 = new global::Fuse.Controls.Panel();
        var temp3 = new global::Fuse.iOS.StatusBarConfig();
        var temp4 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp5 = new global::Fuse.Elements.Viewport();
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Fuse.Controls.BottomBarBackground();
        var temp8 = new global::Fuse.Drawing.SolidColor();
        var temp9 = new global::Fuse.Controls.Panel();
        var temp10 = new global::Fuse.Controls.StackPanel();
        var temp11 = new global::Fuse.Controls.StatusBarBackground();
        var temp12 = new global::Fuse.Controls.Panel();
        var temp13 = new global::Icon();
        var temp14 = new global::Fuse.Resources.MultiDensityImageSource();
        var temp15 = new global::Fuse.Resources.FileImageSource();
        var temp16 = new global::Fuse.Resources.FileImageSource();
        var temp17 = new global::Fuse.Gestures.Clicked();
        var temp18 = new global::Fuse.Gestures.SetSwipeActive();
        var temp19 = new global::Icon();
        var temp20 = new global::Fuse.Resources.MultiDensityImageSource();
        var temp21 = new global::Fuse.Resources.FileImageSource();
        var temp22 = new global::Fuse.Resources.FileImageSource();
        var temp23 = new global::Icon();
        var temp24 = new global::Fuse.Resources.MultiDensityImageSource();
        var temp25 = new global::Fuse.Resources.FileImageSource();
        var temp26 = new global::Fuse.Resources.FileImageSource();
        var temp27 = new global::Fuse.Gestures.SwipingAnimation(swipeGesture);
        var temp28 = new global::Fuse.Animations.Change<float>(menuBtn_Opacity_inst);
        var temp29 = new global::Fuse.Animations.Change<float>(closeBtn_Opacity_inst);
        var temp30 = new global::Fuse.Controls.Panel();
        var temp31 = new global::Fuse.Controls.PageControl();
        var container = new Template(this, this);
        var temp32 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
        temp2.Width = new Uno.UX.Size(0f, Uno.UX.Unit.Unspecified);
        temp2.Height = new Uno.UX.Size(0f, Uno.UX.Unit.Unspecified);
        temp2.Children.Add(temp3);
        temp3.Style = Fuse.Platform.StatusBarStyle.Dark;
        temp4.LineNumber = 7;
        temp4.FileName = "Pages/RotatingPages/RotatingPages.ux";
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Pages/RotatingPages/RotatingPages.js"));
        temp5.CullFace = Uno.Graphics.PolygonFace.Back;
        temp5.Perspective = 300f;
        temp5.Children.Add(temp6);
        temp6.Background = temp8;
        temp6.Children.Add(temp7);
        temp6.Children.Add(swipeGesture);
        temp6.Children.Add(temp9);
        temp6.Children.Add(temp30);
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Bottom);
        temp8.Color = float4(0.9529412f, 0.9529412f, 0.9607843f, 1f);
        swipeGesture.Type = Fuse.Gestures.SwipeType.Active;
        swipeGesture.Direction = Fuse.Gestures.SwipeDirection.Up;
        swipeGesture.Length = 200f;
        swipeGesture.Name = __selector2;
        temp9.Alignment = Fuse.Elements.Alignment.Top;
        temp9.Children.Add(temp10);
        temp10.Children.Add(temp11);
        temp10.Children.Add(temp12);
        temp12.Children.Add(closeBtn);
        temp12.Children.Add(menuBtn);
        temp12.Children.Add(temp23);
        temp12.Children.Add(temp27);
        closeBtn.Alignment = Fuse.Elements.Alignment.Left;
        closeBtn.Opacity = 0f;
        closeBtn.Name = __selector3;
        closeBtn.Children.Add(temp13);
        temp13.Source = temp14;
        temp13.Children.Add(temp17);
        temp14.Sources.Add(temp15);
        temp14.Sources.Add(temp16);
        temp15.Density = 1f;
        temp15.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/ic_close_white_24dp_2x.png"));
        temp16.Density = 2f;
        temp16.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/ic_close_white_24dp_4x.png"));
        temp17.Actions.Add(temp18);
        temp18.Value = false;
        temp18.Target = swipeGesture;
        menuBtn.Alignment = Fuse.Elements.Alignment.Left;
        menuBtn.Name = __selector4;
        menuBtn.Children.Add(temp19);
        temp19.Source = temp20;
        temp20.Sources.Add(temp21);
        temp20.Sources.Add(temp22);
        temp21.Density = 1f;
        temp21.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/ic_menu_white_24dp_2x.png"));
        temp22.Density = 2f;
        temp22.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/ic_menu_white_24dp_4x.png"));
        temp23.Alignment = Fuse.Elements.Alignment.Right;
        temp23.Source = temp24;
        temp24.Sources.Add(temp25);
        temp24.Sources.Add(temp26);
        temp25.Density = 1f;
        temp25.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/ic_shopping_basket_white_24dp_2x.png"));
        temp26.Density = 2f;
        temp26.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/ic_shopping_basket_white_24dp_4x.png"));
        temp27.Animators.Add(temp28);
        temp27.Animators.Add(temp29);
        temp28.Value = 0f;
        temp29.Value = 1f;
        temp30.Children.Add(temp31);
        temp31.Transition = Fuse.Controls.NavigationControlTransition.None;
        temp31.Children.Add(temp);
        temp.Templates.Add(container);
        temp.Bindings.Add(temp32);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(swipeGesture);
        __g_nametable.Objects.Add(closeBtn);
        __g_nametable.Objects.Add(menuBtn);
        this.Children.Add(temp2);
        this.Children.Add(temp4);
        this.Children.Add(temp5);
    }
    static global::Uno.UX.Selector __selector0 = "Opacity";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "swipeGesture";
    static global::Uno.UX.Selector __selector3 = "closeBtn";
    static global::Uno.UX.Selector __selector4 = "menuBtn";
}
