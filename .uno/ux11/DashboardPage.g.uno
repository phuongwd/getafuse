[Uno.Compiler.UxGenerated]
public partial class DashboardPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly DashboardPage __parent;
        [Uno.WeakReference] internal readonly DashboardPage __parentInstance;
        public Template(DashboardPage parent, DashboardPage parentInstance): base("home", false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::HomePage(__parent.router);
            var temp = new global::Fuse.Drawing.StaticSolidColor(float4(0.9960784f, 0.9960784f, 0.9960784f, 1f));
            __self.Name = __selector0;
            __self.Background = temp;
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "home";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly DashboardPage __parent;
        [Uno.WeakReference] internal readonly DashboardPage __parentInstance;
        public Template1(DashboardPage parent, DashboardPage parentInstance): base("explore", false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::ExplorePage(__parent.router);
            var temp = new global::Fuse.Drawing.StaticSolidColor(float4(0.9960784f, 0.9960784f, 0.9960784f, 1f));
            __self.Name = __selector0;
            __self.Background = temp;
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "explore";
    }
    internal global::Fuse.Controls.Panel page1Tab;
    internal global::Fuse.Reactive.EventBinding temp_eb4;
    internal global::Fuse.Controls.Panel page2Tab;
    internal global::Fuse.Reactive.EventBinding temp_eb5;
    internal global::Fuse.Controls.Panel page3Tab;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "page1Tab",
        "temp_eb4",
        "page2Tab",
        "temp_eb5",
        "page3Tab"
    };
    static DashboardPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public DashboardPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Data("goHome");
        var temp1 = new global::Fuse.Reactive.Data("goExplore");
        var temp2 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp3 = new global::Fuse.Controls.DockPanel();
        var temp4 = new global::Fuse.Controls.BottomBarBackground();
        var temp5 = new global::Fuse.Controls.Grid();
        page1Tab = new global::Fuse.Controls.Panel();
        var temp6 = new global::Tab();
        var temp7 = new global::Fuse.Gestures.Clicked();
        temp_eb4 = new global::Fuse.Reactive.EventBinding(temp, __g_nametable);
        page2Tab = new global::Fuse.Controls.Panel();
        var temp8 = new global::Tab();
        var temp9 = new global::Fuse.Gestures.Clicked();
        temp_eb5 = new global::Fuse.Reactive.EventBinding(temp1, __g_nametable);
        page3Tab = new global::Fuse.Controls.Panel();
        var temp10 = new global::Tab();
        var temp11 = new global::Fuse.Gestures.Clicked();
        var temp12 = new global::Fuse.Drawing.StaticSolidColor(float4(0.7411765f, 0.7647059f, 0.7803922f, 1f));
        var temp13 = new global::Fuse.Controls.Navigator();
        var home = new Template(this, this);
        var explore = new Template1(this, this);
        temp2.Code = "\n        function goHome() {\n            router.push('home');\n        }\n        function goExplore() {\n            console.log('fgsa');\n            router.push('explore');\n        }\n\n        module.exports = {\n            goHome: goHome\n        }\n    ";
        temp2.LineNumber = 2;
        temp2.FileName = "Pages/Dashboard/DashboardPage.ux";
        temp3.Children.Add(temp4);
        temp3.Children.Add(temp5);
        temp3.Children.Add(temp13);
        global::Fuse.Controls.DockPanel.SetDock(temp4, Fuse.Layouts.Dock.Bottom);
        temp5.ColumnCount = 3;
        temp5.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.DockPanel.SetDock(temp5, Fuse.Layouts.Dock.Bottom);
        temp5.Background = temp12;
        temp5.Children.Add(page1Tab);
        temp5.Children.Add(page2Tab);
        temp5.Children.Add(page3Tab);
        page1Tab.Name = __selector0;
        page1Tab.Children.Add(temp6);
        temp6.Text = "Today";
        temp6.Children.Add(temp7);
        temp7.Handler += temp_eb4.OnEvent;
        temp7.Bindings.Add(temp_eb4);
        page2Tab.Name = __selector1;
        page2Tab.Children.Add(temp8);
        temp8.Text = "Explore";
        temp8.Children.Add(temp9);
        temp9.Handler += temp_eb5.OnEvent;
        temp9.Bindings.Add(temp_eb5);
        page3Tab.Name = __selector2;
        page3Tab.Children.Add(temp10);
        temp10.Text = "My Summaries";
        temp10.Children.Add(temp11);
        temp13.DefaultPath = "home";
        temp13.ClipToBounds = true;
        temp13.Templates.Add(home);
        temp13.Templates.Add(explore);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(page1Tab);
        __g_nametable.Objects.Add(temp_eb4);
        __g_nametable.Objects.Add(page2Tab);
        __g_nametable.Objects.Add(temp_eb5);
        __g_nametable.Objects.Add(page3Tab);
        this.Children.Add(temp2);
        this.Children.Add(temp3);
    }
    static global::Uno.UX.Selector __selector0 = "page1Tab";
    static global::Uno.UX.Selector __selector1 = "page2Tab";
    static global::Uno.UX.Selector __selector2 = "page3Tab";
}
