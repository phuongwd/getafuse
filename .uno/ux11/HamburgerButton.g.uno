[Uno.Compiler.UxGenerated]
public partial class HamburgerButton: Fuse.Controls.Rectangle
{
    bool _field_IsOpen;
    [global::Uno.UX.UXOriginSetter("SetIsOpen")]
    public bool IsOpen
    {
        get { return _field_IsOpen; }
        set { SetIsOpen(value, null); }
    }
    public void SetIsOpen(bool value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_IsOpen)
        {
            _field_IsOpen = value;
            OnPropertyChanged("IsOpen", origin);
        }
    }
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<bool> this_IsOpen_inst;
    internal global::Fuse.Controls.Rectangle top;
    internal global::Fuse.Controls.Rectangle mid;
    internal global::Fuse.Controls.Rectangle bot;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "top",
        "mid",
        "bot"
    };
    static HamburgerButton()
    {
    }
    [global::Uno.UX.UXConstructor]
    public HamburgerButton()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.This();
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, getAFuse_accessor_HamburgerButton_IsOpen.Singleton);
        this_IsOpen_inst = new getAFuse_HamburgerButton_IsOpen_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp3 = new global::Fuse.Animations.Move();
        var temp4 = new global::Fuse.Animations.Move();
        var temp5 = new global::Fuse.Animations.Rotate();
        var temp6 = new global::Fuse.Animations.Rotate();
        var temp7 = new global::Fuse.Animations.Scale();
        var temp8 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Read);
        var temp9 = new global::Fuse.Controls.StackPanel();
        top = new global::Fuse.Controls.Rectangle();
        mid = new global::Fuse.Controls.Rectangle();
        bot = new global::Fuse.Controls.Rectangle();
        this.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        this.Width = new Uno.UX.Size(56f, Uno.UX.Unit.Unspecified);
        this.Height = new Uno.UX.Size(56f, Uno.UX.Unit.Unspecified);
        temp.Animators.Add(temp3);
        temp.Animators.Add(temp4);
        temp.Animators.Add(temp5);
        temp.Animators.Add(temp6);
        temp.Animators.Add(temp7);
        temp.Bindings.Add(temp8);
        temp3.Y = 6f;
        temp3.Duration = 0.6;
        temp3.DelayBack = 0;
        temp3.Delay = 0.1;
        temp3.Target = top;
        temp3.Easing = Fuse.Animations.Easing.CubicInOut;
        temp4.Y = -6f;
        temp4.Duration = 0.6;
        temp4.DelayBack = 0;
        temp4.Delay = 0.1;
        temp4.Target = bot;
        temp4.Easing = Fuse.Animations.Easing.CubicInOut;
        temp5.Degrees = 135f;
        temp5.Duration = 0.6;
        temp5.DelayBack = 0;
        temp5.Delay = 0.1;
        temp5.Target = top;
        temp5.Easing = Fuse.Animations.Easing.CubicInOut;
        temp6.Degrees = 45f;
        temp6.Duration = 0.6;
        temp6.DelayBack = 0;
        temp6.Delay = 0.1;
        temp6.Target = bot;
        temp6.Easing = Fuse.Animations.Easing.CubicInOut;
        temp7.X = 0f;
        temp7.Duration = 0.5;
        temp7.Target = mid;
        temp7.Easing = Fuse.Animations.Easing.QuarticInOut;
        temp9.ItemSpacing = 4f;
        temp9.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp9.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp9.Children.Add(top);
        temp9.Children.Add(mid);
        temp9.Children.Add(bot);
        top.CornerRadius = float4(1f, 1f, 1f, 1f);
        top.Color = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
        top.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        top.Name = __selector2;
        mid.CornerRadius = float4(1f, 1f, 1f, 1f);
        mid.Color = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
        mid.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        mid.Name = __selector3;
        bot.CornerRadius = float4(1f, 1f, 1f, 1f);
        bot.Color = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
        bot.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        bot.Name = __selector4;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(top);
        __g_nametable.Objects.Add(mid);
        __g_nametable.Objects.Add(bot);
        __g_nametable.Properties.Add(this_IsOpen_inst);
        this.Children.Add(temp);
        this.Children.Add(temp9);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "IsOpen";
    static global::Uno.UX.Selector __selector2 = "top";
    static global::Uno.UX.Selector __selector3 = "mid";
    static global::Uno.UX.Selector __selector4 = "bot";
}
