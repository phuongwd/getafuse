[Uno.Compiler.UxGenerated]
public partial class StyledLabel: Fuse.Controls.Text
{
    static StyledLabel()
    {
    }
    [global::Uno.UX.UXConstructor]
    public StyledLabel()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        this.FontSize = 17f;
        this.TextAlignment = Fuse.Controls.TextAlignment.Center;
        this.Color = float4(0.3333333f, 0.4313726f, 0.4784314f, 1f);
        this.Font = global::RotatingPages.Hind;
    }
}
