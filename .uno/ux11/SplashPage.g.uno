[Uno.Compiler.UxGenerated]
public partial class SplashPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    internal global::Fuse.Reactive.EventBinding temp_eb27;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb27"
    };
    static SplashPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SplashPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Data("pageOnActivated");
        var temp1 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp2 = new global::Fuse.Controls.Panel();
        var temp3 = new global::Fuse.Controls.Image();
        var temp4 = new global::Fuse.Navigation.Activated();
        var temp5 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb27 = new global::Fuse.Reactive.EventBinding(temp, __g_nametable);
        global::Fuse.Controls.NavigationControl.SetTransition(this, Fuse.Controls.NavigationControlTransition.None);
        temp1.Code = "\n    var Timer = require(\"FuseJS/Timer\");\n    function pageOnActivated() {\n        console.log('pageOnActivated');\n        var tokens = localStorage.getItem('gaTokens');\n\n        Timer.create(function() {\n            console.log(\"This will run once, after 3 seconds\");\n            if(tokens) {\n                router.push('login');\n            } else {\n                router.push('login');\n            }\n        }, 3000, false);\n    }\n    \n    module.exports = {\n        pageOnActivated: pageOnActivated\n    }\n    ";
        temp1.LineNumber = 2;
        temp1.FileName = "Pages/Splash/SplashPage.ux";
        temp2.Children.Add(temp3);
        temp3.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/Default~iphone.png"));
        temp4.Actions.Add(temp5);
        temp4.Bindings.Add(temp_eb27);
        temp5.Handler += temp_eb27.OnEvent;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb27);
        this.Children.Add(temp1);
        this.Children.Add(temp2);
        this.Children.Add(temp4);
    }
}
