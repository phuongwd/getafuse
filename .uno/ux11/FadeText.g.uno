[Uno.Compiler.UxGenerated]
public partial class FadeText: Fuse.Controls.Text
{
    global::Uno.UX.Property<float> this_Opacity_inst;
    static FadeText()
    {
    }
    [global::Uno.UX.UXConstructor]
    public FadeText()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(this, __selector0);
        var temp = new global::Fuse.Triggers.AddingAnimation();
        var temp1 = new global::Fuse.Animations.Change<float>(this_Opacity_inst);
        var temp2 = new global::Fuse.Triggers.RemovingAnimation();
        var temp3 = new global::Fuse.Animations.Change<float>(this_Opacity_inst);
        this.TextAlignment = Fuse.Controls.TextAlignment.Center;
        this.Alignment = Fuse.Elements.Alignment.Center;
        this.Name = __selector1;
        temp.Animators.Add(temp1);
        temp1.Value = 0f;
        temp1.Duration = 0.2;
        temp1.Delay = 0.2;
        temp2.Animators.Add(temp3);
        temp3.Value = 0f;
        temp3.Duration = 0.2;
        this.Font = global::Watch.DroidSans;
        this.Children.Add(temp);
        this.Children.Add(temp2);
    }
    static global::Uno.UX.Selector __selector0 = "Opacity";
    static global::Uno.UX.Selector __selector1 = "self";
}
