[Uno.Compiler.UxGenerated]
public partial class OverviewPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly OverviewPage __parent;
        [Uno.WeakReference] internal readonly OverviewPage __parentInstance;
        public Template(OverviewPage parent, OverviewPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Separator();
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly OverviewPage __parent;
        [Uno.WeakReference] internal readonly OverviewPage __parentInstance;
        public Template1(OverviewPage parent, OverviewPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::ListItem();
            return __self;
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<Fuse.Elements.Visibility> overlay_Visibility_inst;
    global::Uno.UX.Property<float> overlay_Opacity_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<object> temp3_Items_inst;
    global::Uno.UX.Property<int> temp3_Offset_inst;
    global::Uno.UX.Property<int> temp3_Limit_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb22;
    internal global::Fuse.Controls.Panel overlay;
    internal global::Fuse.Controls.Text overlayText;
    internal global::Fuse.Controls.ScrollView _scrollView;
    internal global::Fuse.Reactive.EventBinding temp_eb23;
    internal global::Fuse.Triggers.Scrolled atEnd;
    internal global::Fuse.Reactive.EventBinding temp_eb24;
    internal global::Fuse.Controls.StackPanel _stackPanel;
    internal global::Fuse.Reactive.EventBinding temp_eb25;
    internal global::Fuse.Reactive.EventBinding temp_eb26;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb22",
        "overlay",
        "overlayText",
        "_scrollView",
        "temp_eb23",
        "atEnd",
        "temp_eb24",
        "_stackPanel",
        "temp_eb25",
        "temp_eb26"
    };
    static OverviewPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public OverviewPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp4 = new global::Fuse.Reactive.Data("goBack");
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("categoryTitle");
        overlay = new global::Fuse.Controls.Panel();
        overlay_Visibility_inst = new getAFuse_FuseElementsElement_Visibility_Property(overlay, __selector1);
        overlay_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(overlay, __selector2);
        var temp1 = new global::Fuse.Triggers.WhileTrue();
        temp1_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp1, __selector0);
        var temp6 = new global::Fuse.Reactive.Data("showOverlay");
        var temp2 = new global::Fuse.Triggers.WhileFalse();
        temp2_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp2, __selector0);
        var temp7 = new global::Fuse.Reactive.Data("showOverlay");
        var temp8 = new global::Fuse.Reactive.Data("decreaseOffset");
        var temp9 = new global::Fuse.Reactive.Data("increaseOffset");
        var temp3 = new global::Fuse.Reactive.Each();
        temp3_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp3, __selector3);
        var temp10 = new global::Fuse.Reactive.Data("list");
        temp3_Offset_inst = new getAFuse_FuseReactiveEach_Offset_Property(temp3, __selector4);
        var temp11 = new global::Fuse.Reactive.Data("offset");
        temp3_Limit_inst = new getAFuse_FuseReactiveEach_Limit_Property(temp3, __selector5);
        var temp12 = new global::Fuse.Reactive.Data("limit");
        var temp13 = new global::Fuse.Reactive.Data("pageOnDeactivated");
        var temp14 = new global::Fuse.Reactive.Data("pageOnActivated");
        var temp15 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp16 = new global::Fuse.Controls.DockPanel();
        var temp17 = new global::Fuse.Controls.Grid();
        var temp18 = new global::Fuse.Controls.Panel();
        var temp19 = new global::Fuse.Controls.StatusBarBackground();
        var temp20 = new global::Fuse.Controls.Panel();
        var temp21 = new global::Fuse.Controls.Grid();
        var temp22 = new global::Fuse.Controls.Button();
        var temp23 = new global::Fuse.Controls.Image();
        var temp24 = new global::Fuse.Controls.Text();
        temp_eb22 = new global::Fuse.Reactive.EventBinding(temp4, __g_nametable);
        var temp25 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp5, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp26 = new global::Fuse.Drawing.StaticSolidColor(float4(0f, 0.682353f, 0.9372549f, 1f));
        overlayText = new global::Fuse.Controls.Text();
        var temp27 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp28 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(overlay_Visibility_inst);
        var temp29 = new global::Fuse.Animations.Change<float>(overlay_Opacity_inst);
        var temp30 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp6, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp31 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(overlay_Visibility_inst);
        var temp32 = new global::Fuse.Animations.Change<float>(overlay_Opacity_inst);
        var temp33 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp7, __g_nametable, Fuse.Reactive.BindingMode.Default);
        _scrollView = new global::Fuse.Controls.ScrollView();
        var temp34 = new global::Fuse.Triggers.Scrolled();
        temp_eb23 = new global::Fuse.Reactive.EventBinding(temp8, __g_nametable);
        atEnd = new global::Fuse.Triggers.Scrolled();
        temp_eb24 = new global::Fuse.Reactive.EventBinding(temp9, __g_nametable);
        _stackPanel = new global::Fuse.Controls.StackPanel();
        var temp35 = new Template(this, this);
        var temp36 = new Template1(this, this);
        var temp37 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp10, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp38 = new global::Fuse.Reactive.DataBinding(temp3_Offset_inst, temp11, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp39 = new global::Fuse.Reactive.DataBinding(temp3_Limit_inst, temp12, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp40 = new global::Fuse.Navigation.Deactivated();
        var temp41 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb25 = new global::Fuse.Reactive.EventBinding(temp13, __g_nametable);
        var temp42 = new global::Fuse.Navigation.Activated();
        var temp43 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb26 = new global::Fuse.Reactive.EventBinding(temp14, __g_nametable);
        temp15.Code = "\n    var Observable = require(\"FuseJS/Observable\");\n    var showOverlay = Observable(false);\n    var category = this.Parameter;\n    var isPageDeactivated = false;\n\n    var categoryId = category.map(function (x) {\n      return x.id;\n    });\n    var categoryTitle = category.map(function (x) {\n      return x.caption;\n    });\n\n    var images = [\n      \"eWFdaPRFjwE\",\"_FjegPI89aU\",\"_RBcxo9AU-U\",\"PQvRco_SnpI\",\"6hxvm0NzYP8\",\n      \"b-yEdfrvQ50\",\"pHANr-CpbYM\",\"45FJgZMXCK8\",\"9wociMvaquU\",\"tI_Odb7ZU6M\",\n      \"o0RZkkL072U\",\"N-sxA8vEGDk\",\"324ovGl1BwM\",\"RSOxw9X-suY\",\"qv5yb436qRI\",\n      \"iWFRUyqpCbQ\",\"ZJ4yhJFIzaY\",\"ze0tqwj86S4\",\"gQR4STZ24kM\",\"xMYnPo53ukE\"\n    ];\n\n    var list = Observable();\n    var offset = Observable(0);\n    var limit = 20;\n    var currentPage = 0;\n\n    function increaseOffset() {\n      changeOffset(5);\n    }\n\n    function decreaseOffset() {\n      changeOffset(-5);\n    }\n\n    function changeOffset(diff) {\n\n      if(isPageDeactivated) return;\n      var nextOffset = offset.value + diff;\n      if (nextOffset >= 0) {\n        offset.value = nextOffset;\n        if ((list.length - nextOffset) <= limit) {\n          loadItems().then(function() {\n            currentPage += 1;\n            atEnd.check();\n          });\n        }\n      }\n    }\n\n    function loadItems(isShowLoading) {\n      // POST /api/{apiVersion}/mobile/explore/categories/{category_id}\n      var p = new Promise(function(resolve) {\n        // var items = images.map(function(i) {\n          // return {image: \"https://source.unsplash.com/\" + i + \"/416x208\"};\n        // });\n        getOverview(isShowLoading, function(items) {\n          list.addAll(items);\n          resolve();\n       });\n        // list.addAll(items);\n        // resolve();\n      });\n      return p;\n    }\n\n    function getOverview(isShowLoading, successCallbacks) {\n          var tokens = localStorage.getItem('gaTokens');\n          tokens = JSON.parse(tokens);\n\n          var filters = {\n            \"audioOnly\": false,\n            \"sorting\": \"ONLINE_DATE\"\n          };\n          var params = {\n            categoryId: categoryId.value,\n            page: currentPage,\n            maxNumbers: 100\n          };\n\n          var myRequest = new Request('https://www.getabstract.com/api/v7/mobile/explore/categories/'  + params.categoryId + '?page='+ params.page + '&max_numbers=' + params.maxNumbers, {\n              method: 'POST', \n              headers: {\n                  \"Authorization\": \"Bearer \" + tokens.access_token,\n                  'Accept': 'application/json, text/javascript, */*; q=0.01',\n                  'Content-Type': 'application/json',\n                  \"Device-Id\": \"CC3AC87A-C4B5-4806-97EF-34534543\",\n                  \"Application\":  \"APP_PHONE\",\n                  \"OS\": \"IOS\",\n                  \"Version\":  \"7.9.0\"\n              },\n              body: JSON.stringify(filters)\n          });\n\n          if(isShowLoading) {\n            showOverlay.value = true;\n          }\n          fetch(myRequest).then(function(response) {\n              status = response.status;  // Get the HTTP status code\n              response_ok = response.ok; // Is response.status in the 200-range?\n              // console.log('response');\n              // console.log(JSON.stringify(response));\n\n              return response.json();    // This returns a promise\n          }).then(function(responseObject) {\n              // data.replaceAll(responseObject.category);\n              // console.log(JSON.stringify(responseObject));\n              showOverlay.value = false;\n              successCallbacks && successCallbacks(responseObject.summaries);\n              \n              // list.addAll(responseObject.summaries);\n              // Do something with the result\n          }).catch(function(err) {\n              showOverlay.value = false;\n              console.log('errrorrr');\n              conosole.log(err);\n              // An error occurred somewhere in the Promise chain\n          });\n      }\n    \n    function goBack() {\n      showOverlay.value = false;\n      router.goBack();\n    }\n\n    function pageOnDeactivated() {\n      isPageDeactivated = true;\n      currentPage = 0;\n\n      list.replaceAll([]);\n    }\n\n    function pageOnActivated() {\n      isPageDeactivated = false;\n      loadItems(true);\n    }\n\n    module.exports = {\n      list: list,\n      categoryTitle: categoryTitle,\n      categoryId: categoryId,\n      offset: offset,\n      limit: limit,\n      increaseOffset: increaseOffset,\n      decreaseOffset: decreaseOffset,\n      goBack: goBack,\n      pageOnDeactivated: pageOnDeactivated,\n      showOverlay: showOverlay,\n      pageOnActivated: pageOnActivated\n    }\n  ";
        temp15.LineNumber = 3;
        temp15.FileName = "Pages/Overview/OverviewPage.ux";
        temp16.Children.Add(temp17);
        temp16.Children.Add(overlay);
        temp16.Children.Add(temp1);
        temp16.Children.Add(temp2);
        temp16.Children.Add(_scrollView);
        temp16.Children.Add(temp40);
        temp16.Children.Add(temp42);
        temp17.Rows = "auto,1*";
        temp17.Height = new Uno.UX.Size(70f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.DockPanel.SetDock(temp17, Fuse.Layouts.Dock.Top);
        temp17.Background = temp26;
        temp17.Children.Add(temp18);
        temp17.Children.Add(temp20);
        temp18.Color = float4(0f, 0.682353f, 0.9372549f, 1f);
        global::Fuse.Controls.Grid.SetRow(temp18, 0);
        temp18.Children.Add(temp19);
        temp20.Color = float4(0f, 0.682353f, 0.9372549f, 1f);
        global::Fuse.Controls.Grid.SetRow(temp20, 1);
        temp20.Children.Add(temp21);
        temp21.Columns = "auto, 1*";
        temp21.Children.Add(temp22);
        temp21.Children.Add(temp);
        temp22.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp22.Alignment = Fuse.Elements.Alignment.Left;
        temp22.Padding = float4(10f, 0f, 0f, 0f);
        global::Fuse.Gestures.Clicked.AddHandler(temp22, temp_eb22.OnEvent);
        temp22.Children.Add(temp23);
        temp22.Children.Add(temp24);
        temp22.Bindings.Add(temp_eb22);
        temp23.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp23.Alignment = Fuse.Elements.Alignment.Left;
        temp23.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/back-icon.png"));
        temp24.Value = "Back";
        temp24.Color = float4(1f, 1f, 1f, 1f);
        temp24.Alignment = Fuse.Elements.Alignment.Center;
        temp24.Margin = float4(20f, 0f, 20f, 0f);
        temp.Color = float4(1f, 1f, 1f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Bindings.Add(temp25);
        overlay.Visibility = Fuse.Elements.Visibility.Hidden;
        overlay.Margin = float4(0f, 70f, 0f, 0f);
        overlay.Opacity = 0f;
        overlay.Layer = Fuse.Layer.Overlay;
        overlay.Name = __selector6;
        overlay.Background = temp27;
        overlay.Children.Add(overlayText);
        overlayText.Value = "Loading...";
        overlayText.Color = float4(0f, 0f, 0f, 1f);
        overlayText.Alignment = Fuse.Elements.Alignment.Center;
        overlayText.Name = __selector7;
        temp1.Animators.Add(temp28);
        temp1.Animators.Add(temp29);
        temp1.Bindings.Add(temp30);
        temp28.Value = Fuse.Elements.Visibility.Visible;
        temp29.Value = 1f;
        temp29.Duration = 0.6;
        temp2.Animators.Add(temp31);
        temp2.Animators.Add(temp32);
        temp2.Bindings.Add(temp33);
        temp31.Value = Fuse.Elements.Visibility.Hidden;
        temp31.Duration = 0.4;
        temp32.Value = 0f;
        temp32.Duration = 0.6;
        _scrollView.LayoutMode = Fuse.Controls.ScrollViewLayoutMode.PreserveVisual;
        _scrollView.Name = __selector8;
        _scrollView.Children.Add(temp34);
        _scrollView.Children.Add(atEnd);
        _scrollView.Children.Add(_stackPanel);
        temp34.To = Fuse.Triggers.ScrolledWhere.Start;
        temp34.Within = 104f;
        temp34.Handler += temp_eb23.OnEvent;
        temp34.Bindings.Add(temp_eb23);
        atEnd.To = Fuse.Triggers.ScrolledWhere.End;
        atEnd.Within = 104f;
        atEnd.Name = __selector9;
        atEnd.Handler += temp_eb24.OnEvent;
        atEnd.Bindings.Add(temp_eb24);
        _stackPanel.ItemSpacing = 8f;
        _stackPanel.Margin = float4(8f, 8f, 8f, 8f);
        _stackPanel.Name = __selector10;
        _stackPanel.Children.Add(temp3);
        temp3.Templates.Add(temp35);
        temp3.Templates.Add(temp36);
        temp3.Bindings.Add(temp37);
        temp3.Bindings.Add(temp38);
        temp3.Bindings.Add(temp39);
        temp40.Actions.Add(temp41);
        temp40.Bindings.Add(temp_eb25);
        temp41.Handler += temp_eb25.OnEvent;
        temp42.Actions.Add(temp43);
        temp42.Bindings.Add(temp_eb26);
        temp43.Handler += temp_eb26.OnEvent;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb22);
        __g_nametable.Objects.Add(overlay);
        __g_nametable.Objects.Add(overlayText);
        __g_nametable.Objects.Add(_scrollView);
        __g_nametable.Objects.Add(temp_eb23);
        __g_nametable.Objects.Add(atEnd);
        __g_nametable.Objects.Add(temp_eb24);
        __g_nametable.Objects.Add(_stackPanel);
        __g_nametable.Objects.Add(temp_eb25);
        __g_nametable.Objects.Add(temp_eb26);
        this.Children.Add(temp15);
        this.Children.Add(temp16);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Visibility";
    static global::Uno.UX.Selector __selector2 = "Opacity";
    static global::Uno.UX.Selector __selector3 = "Items";
    static global::Uno.UX.Selector __selector4 = "Offset";
    static global::Uno.UX.Selector __selector5 = "Limit";
    static global::Uno.UX.Selector __selector6 = "overlay";
    static global::Uno.UX.Selector __selector7 = "overlayText";
    static global::Uno.UX.Selector __selector8 = "_scrollView";
    static global::Uno.UX.Selector __selector9 = "atEnd";
    static global::Uno.UX.Selector __selector10 = "_stackPanel";
}
