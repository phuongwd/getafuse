[Uno.Compiler.UxGenerated]
public partial class ExplorePage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly ExplorePage __parent;
        [Uno.WeakReference] internal readonly ExplorePage __parentInstance;
        public Template(ExplorePage parent, ExplorePage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Separator();
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly ExplorePage __parent;
        [Uno.WeakReference] internal readonly ExplorePage __parentInstance;
        public Template1(ExplorePage parent, ExplorePage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb6;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "temp_eb6"
        };
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("caption");
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp2 = new global::Fuse.Reactive.Data("goOverview");
            var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp_eb6 = new global::Fuse.Reactive.EventBinding(temp2, __g_nametable);
            __self.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
            global::Fuse.Gestures.Clicked.AddHandler(__self, temp_eb6.OnEvent);
            temp.Color = float4(0f, 0f, 0f, 1f);
            temp.Margin = float4(15f, 15f, 15f, 15f);
            temp.Bindings.Add(temp3);
            __g_nametable.Objects.Add(temp_eb6);
            __self.Children.Add(temp);
            __self.Bindings.Add(temp_eb6);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
    }
    global::Uno.UX.Property<Fuse.Elements.Visibility> overlay_Visibility_inst;
    global::Uno.UX.Property<float> overlay_Opacity_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<object> temp2_Items_inst;
    internal global::Fuse.iOS.StatusBarConfig statusBarConfig;
    internal global::Fuse.Controls.Panel overlay;
    internal global::Fuse.Controls.Text overlayText;
    internal global::Fuse.Reactive.EventBinding temp_eb7;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "statusBarConfig",
        "overlay",
        "overlayText",
        "temp_eb7"
    };
    static ExplorePage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ExplorePage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        overlay = new global::Fuse.Controls.Panel();
        overlay_Visibility_inst = new getAFuse_FuseElementsElement_Visibility_Property(overlay, __selector0);
        overlay_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(overlay, __selector1);
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp, __selector2);
        var temp3 = new global::Fuse.Reactive.Data("showOverlay");
        var temp1 = new global::Fuse.Triggers.WhileFalse();
        temp1_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp1, __selector2);
        var temp4 = new global::Fuse.Reactive.Data("showOverlay");
        var temp2 = new global::Fuse.Reactive.Each();
        temp2_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp2, __selector3);
        var temp5 = new global::Fuse.Reactive.Data("dataSource");
        var temp6 = new global::Fuse.Reactive.Data("pageOnActivated");
        var temp7 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        statusBarConfig = new global::Fuse.iOS.StatusBarConfig();
        overlayText = new global::Fuse.Controls.Text();
        var temp8 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp9 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(overlay_Visibility_inst);
        var temp10 = new global::Fuse.Animations.Change<float>(overlay_Opacity_inst);
        var temp11 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(overlay_Visibility_inst);
        var temp13 = new global::Fuse.Animations.Change<float>(overlay_Opacity_inst);
        var temp14 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp4, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp15 = new global::Fuse.Controls.ScrollView();
        var temp16 = new global::Fuse.Controls.StackPanel();
        var temp17 = new global::Fuse.Controls.Panel();
        var temp18 = new Template(this, this);
        var temp19 = new Template1(this, this);
        var temp20 = new global::Fuse.Reactive.DataBinding(temp2_Items_inst, temp5, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp21 = new global::Fuse.Navigation.Activated();
        var temp22 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb7 = new global::Fuse.Reactive.EventBinding(temp6, __g_nametable);
        temp7.Code = "\n        var Observable = require(\"FuseJS/Observable\");\n        var data = Observable();\n        var showOverlay = Observable(false);\n        var initData = false;\n        pageOnActivated();\n        function pageOnActivated() {\n            if(initData) return;\n            \n            var tokens = localStorage.getItem('gaTokens');\n            tokens = JSON.parse(tokens);\n            var myRequest = new Request('https://www.getabstract.com/api/v7/mobile/menu', {\n                method: 'GET', \n                headers: {\n                    \"Authorization\": \"Bearer \" + tokens.access_token,\n                    'Accept': 'application/json, text/javascript, */*; q=0.01',\n                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',\n                    \"Device-Id\": \"CC3AC87A-C4B5-4806-97EF-34534543\",\n                    \"Application\":  \"APP_PHONE\",\n                    \"OS\": \"IOS\",\n                    \"Version\":  \"7.9.0\"\n                }\n            });\n            showOverlay.value = true;\n            fetch(myRequest).then(function(response) {\n                status = response.status;  // Get the HTTP status code\n                response_ok = response.ok; // Is response.status in the 200-range?\n                // console.log('response');\n                // console.log(JSON.stringify(response));\n\n                return response.json();    // This returns a promise\n            }).then(function(responseObject) {\n                data.replaceAll(responseObject.category);\n                initData = true;\n                showOverlay.value = false;\n                // console.log(JSON.stringify(responseObject));\n                // Do something with the result\n            }).catch(function(err) {\n                console.log('errrorrr');\n                conosole.log(err);\n                // An error occurred somewhere in the Promise chain\n            });\n        }\n\n        function goOverview(arg) {\n            var cate = arg.data;\n            router.push('overview', cate);\n        }\n\n        module.exports = {\n            dataSource: data,\n            goOverview: goOverview,\n            showOverlay: showOverlay\n        }\n    ";
        temp7.LineNumber = 2;
        temp7.FileName = "Pages/Explore/ExplorePage.ux";
        statusBarConfig.Style = Fuse.Platform.StatusBarStyle.Dark;
        statusBarConfig.Name = __selector4;
        overlay.Visibility = Fuse.Elements.Visibility.Hidden;
        overlay.Opacity = 0f;
        overlay.Layer = Fuse.Layer.Overlay;
        overlay.Name = __selector5;
        overlay.Background = temp8;
        overlay.Children.Add(overlayText);
        overlayText.Value = "Loading...";
        overlayText.Color = float4(0f, 0f, 0f, 1f);
        overlayText.Alignment = Fuse.Elements.Alignment.Center;
        overlayText.Name = __selector6;
        temp.Animators.Add(temp9);
        temp.Animators.Add(temp10);
        temp.Bindings.Add(temp11);
        temp9.Value = Fuse.Elements.Visibility.Visible;
        temp10.Value = 1f;
        temp10.Duration = 0.6;
        temp1.Animators.Add(temp12);
        temp1.Animators.Add(temp13);
        temp1.Bindings.Add(temp14);
        temp12.Value = Fuse.Elements.Visibility.Hidden;
        temp12.Duration = 0.4;
        temp13.Value = 0f;
        temp13.Duration = 0.6;
        temp15.Children.Add(temp16);
        temp16.Children.Add(temp17);
        temp16.Children.Add(temp2);
        temp17.Margin = float4(0f, 20f, 0f, 0f);
        temp2.Templates.Add(temp18);
        temp2.Templates.Add(temp19);
        temp2.Bindings.Add(temp20);
        temp21.Actions.Add(temp22);
        temp21.Bindings.Add(temp_eb7);
        temp22.Handler += temp_eb7.OnEvent;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(statusBarConfig);
        __g_nametable.Objects.Add(overlay);
        __g_nametable.Objects.Add(overlayText);
        __g_nametable.Objects.Add(temp_eb7);
        this.Children.Add(temp7);
        this.Children.Add(statusBarConfig);
        this.Children.Add(overlay);
        this.Children.Add(temp);
        this.Children.Add(temp1);
        this.Children.Add(temp15);
        this.Children.Add(temp21);
    }
    static global::Uno.UX.Selector __selector0 = "Visibility";
    static global::Uno.UX.Selector __selector1 = "Opacity";
    static global::Uno.UX.Selector __selector2 = "Value";
    static global::Uno.UX.Selector __selector3 = "Items";
    static global::Uno.UX.Selector __selector4 = "statusBarConfig";
    static global::Uno.UX.Selector __selector5 = "overlay";
    static global::Uno.UX.Selector __selector6 = "overlayText";
}
