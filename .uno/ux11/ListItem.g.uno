[Uno.Compiler.UxGenerated]
public partial class ListItem: Fuse.Controls.Panel
{
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly ListItem __parent;
        [Uno.WeakReference] internal readonly ListItem __parentInstance;
        public Template(ListItem parent, ListItem parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<float> __self_Opacity_inst;
        global::Uno.UX.Property<bool> showLoadingText_Value_inst;
        global::Uno.UX.Property<float> thumbnail_Opacity_inst;
        global::Uno.UX.Property<string> thumbnail_Url_inst;
        global::Uno.UX.Property<string> temp_Value_inst;
        internal global::Fuse.Controls.Image thumbnail;
        internal global::Fuse.Triggers.WhileTrue showLoadingText;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "thumbnail",
            "showLoadingText",
            "imageHolder"
        };
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Rectangle();
            __self_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(__self, __selector0);
            var showLoadingText = new global::Fuse.Triggers.WhileTrue();
            showLoadingText_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(showLoadingText, __selector1);
            var thumbnail = new global::Fuse.Controls.Image();
            thumbnail_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(thumbnail, __selector0);
            thumbnail_Url_inst = new getAFuse_FuseControlsImage_Url_Property(thumbnail, __selector2);
            var temp1 = new global::Fuse.Reactive.Data("thumbnail_medium");
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector1);
            var temp2 = new global::Fuse.Reactive.Data("title");
            var temp3 = new global::Fuse.Triggers.AddingAnimation();
            var temp4 = new global::Fuse.Animations.Change<float>(__self_Opacity_inst);
            var temp5 = new global::Fuse.Controls.Grid();
            var temp6 = new global::Fuse.Controls.Panel();
            var temp7 = new global::Fuse.Triggers.WhileLoading();
            var temp8 = new global::Fuse.Animations.Change<bool>(showLoadingText_Value_inst);
            var temp9 = new global::Fuse.Animations.Change<float>(thumbnail_Opacity_inst);
            var temp10 = new global::Fuse.Triggers.WhileCompleted();
            var temp11 = new global::Fuse.Animations.Change<float>(thumbnail_Opacity_inst);
            var temp12 = new global::Fuse.Reactive.DataBinding(thumbnail_Url_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp13 = new global::Fuse.Controls.Text();
            var temp14 = new global::Fuse.Controls.Panel();
            var temp15 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp16 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            __self.CornerRadius = float4(2f, 2f, 2f, 2f);
            __self.Opacity = 1f;
            __self.Name = __selector3;
            temp3.Animators.Add(temp4);
            temp4.Value = 0f;
            temp4.Duration = 0.32;
            temp4.Delay = 0.8;
            temp5.RowCount = 1;
            temp5.Columns = "133, 1*";
            temp5.Background = temp16;
            temp5.Children.Add(temp6);
            temp5.Children.Add(temp14);
            temp6.Width = new Uno.UX.Size(133f, Uno.UX.Unit.Unspecified);
            temp6.Alignment = Fuse.Elements.Alignment.Center;
            global::Fuse.Controls.Grid.SetRow(temp6, 0);
            global::Fuse.Controls.Grid.SetColumn(temp6, 0);
            temp6.Children.Add(thumbnail);
            temp6.Children.Add(showLoadingText);
            thumbnail.Width = new Uno.UX.Size(133f, Uno.UX.Unit.Unspecified);
            thumbnail.Name = __selector4;
            thumbnail.MemoryPolicy = Fuse.Resources.MemoryPolicy.UnloadUnused;
            thumbnail.Children.Add(temp7);
            thumbnail.Children.Add(temp10);
            thumbnail.Bindings.Add(temp12);
            temp7.Animators.Add(temp8);
            temp7.Animators.Add(temp9);
            temp8.Value = true;
            temp9.Value = 0f;
            temp9.Duration = 0.32;
            temp10.Animators.Add(temp11);
            temp11.Value = 1f;
            temp11.Duration = 0.32;
            showLoadingText.Name = __selector5;
            showLoadingText.Nodes.Add(temp13);
            temp13.Value = "Loading...";
            temp13.TextAlignment = Fuse.Controls.TextAlignment.Center;
            temp14.Alignment = Fuse.Elements.Alignment.Top;
            temp14.Padding = float4(10f, 0f, 10f, 0f);
            global::Fuse.Controls.Grid.SetRow(temp14, 0);
            global::Fuse.Controls.Grid.SetColumn(temp14, 1);
            temp14.Children.Add(temp);
            temp.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
            temp.Bindings.Add(temp15);
            __g_nametable.Objects.Add(thumbnail);
            __g_nametable.Objects.Add(showLoadingText);
            __g_nametable.Objects.Add(__self);
            __self.Children.Add(temp3);
            __self.Children.Add(temp5);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Opacity";
        static global::Uno.UX.Selector __selector1 = "Value";
        static global::Uno.UX.Selector __selector2 = "Url";
        static global::Uno.UX.Selector __selector3 = "imageHolder";
        static global::Uno.UX.Selector __selector4 = "thumbnail";
        static global::Uno.UX.Selector __selector5 = "showLoadingText";
    }
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
    };
    static ListItem()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ListItem()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Deferred();
        var imageHolder = new Template(this, this);
        var temp1 = new global::Fuse.Controls.Rectangle();
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        this.Height = new Uno.UX.Size(104f, Uno.UX.Unit.Unspecified);
        temp.Templates.Add(imageHolder);
        temp1.CornerRadius = float4(2f, 2f, 2f, 2f);
        temp1.Color = float4(0f, 0f, 0f, 0.2f);
        __g_nametable.This = this;
        this.Children.Add(temp);
        this.Children.Add(temp1);
    }
}
