[Uno.Compiler.UxGenerated]
public partial class CubeTransitionPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly CubeTransitionPage __parent;
        [Uno.WeakReference] internal readonly CubeTransitionPage __parentInstance;
        public Template(CubeTransitionPage parent, CubeTransitionPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Rectangle();
            __self.CornerRadius = float4(2f, 2f, 2f, 2f);
            __self.Color = float4(0.9333333f, 0.9333333f, 0.9333333f, 1f);
            __self.Aspect = 1f;
            __self.BoxSizing = Fuse.Elements.Element.BoxSizingMode.FillAspect;
            return __self;
        }
    }
    global::Uno.UX.Property<float> content_Opacity_inst;
    global::Uno.UX.Property<float> menu_Opacity_inst;
    global::Uno.UX.Property<bool> hamburger_IsOpen_inst;
    global::Uno.UX.Property<Fuse.Elements.Element> searchField_Element_LayoutMaster_inst;
    global::Uno.UX.Property<double> transitionToSearchLayout_TargetProgress_inst;
    global::Uno.UX.Property<float> normalMenuItems_Opacity_inst;
    global::Uno.UX.Property<float4> searchInput_PlaceholderColor_inst;
    global::Uno.UX.Property<float> searchCloseButton_Opacity_inst;
    global::Uno.UX.Property<string> searchInput_Value_inst;
    global::Uno.UX.Property<bool> searchFieldActive_Value_inst;
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<bool> menuIsOpen_Value_inst;
    [global::Uno.UX.UXGlobalResource("BackgroundColor")] public static readonly Uno.Float4 BackgroundColor;
    [global::Uno.UX.UXGlobalResource("ForegroundColor")] public static readonly Uno.Float4 ForegroundColor;
    internal global::Fuse.Triggers.WhileTrue menuIsOpen;
    internal global::HamburgerButton hamburger;
    internal global::Fuse.Controls.Panel box;
    internal global::Fuse.Controls.Panel sidePage;
    internal global::Fuse.Triggers.WhileTrue searchFieldActive;
    internal global::Fuse.Triggers.Timeline transitionToSearchLayout;
    internal global::Fuse.Controls.DockPanel searchLayout;
    internal global::Fuse.Controls.Panel searchFieldLayoutMaster;
    internal global::Fuse.Controls.Rectangle searchCloseButton;
    internal global::Fuse.Controls.StackPanel menu;
    [global::Uno.UX.UXGlobalResource("MenuItemHeight")] public static readonly Uno.UX.Size MenuItemHeight;
    internal global::MenuItem searchField;
    internal global::Fuse.Controls.TextInput searchInput;
    internal global::Fuse.Controls.StackPanel normalMenuItems;
    internal global::Fuse.Controls.DockPanel content;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "menuIsOpen",
        "hamburger",
        "box",
        "sidePage",
        "searchFieldActive",
        "transitionToSearchLayout",
        "searchLayout",
        "searchFieldLayoutMaster",
        "searchCloseButton",
        "menu",
        "searchField",
        "searchInput",
        "normalMenuItems",
        "content"
    };
    static CubeTransitionPage()
    {
        BackgroundColor = float4(0.1294118f, 0.1294118f, 0.1294118f, 1f);
        ForegroundColor = float4(0.9254902f, 0.9372549f, 0.945098f, 1f);
        MenuItemHeight = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        global::Uno.UX.Resource.SetGlobalKey(BackgroundColor, "BackgroundColor");
        global::Uno.UX.Resource.SetGlobalKey(ForegroundColor, "ForegroundColor");
        global::Uno.UX.Resource.SetGlobalKey(MenuItemHeight, "MenuItemHeight");
    }
    [global::Uno.UX.UXConstructor]
    public CubeTransitionPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        content = new global::Fuse.Controls.DockPanel();
        content_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(content, __selector0);
        menu = new global::Fuse.Controls.StackPanel();
        menu_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(menu, __selector0);
        hamburger = new global::HamburgerButton();
        hamburger_IsOpen_inst = new getAFuse_HamburgerButton_IsOpen_Property(hamburger, __selector1);
        searchField = new global::MenuItem();
        searchField_Element_LayoutMaster_inst = new getAFuse_FuseElementsElement_ElementLayoutMaster_Property(searchField, __selector2);
        transitionToSearchLayout = new global::Fuse.Triggers.Timeline();
        transitionToSearchLayout_TargetProgress_inst = new getAFuse_FuseTriggersTimeline_TargetProgress_Property(transitionToSearchLayout, __selector3);
        normalMenuItems = new global::Fuse.Controls.StackPanel();
        normalMenuItems_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(normalMenuItems, __selector0);
        searchInput = new global::Fuse.Controls.TextInput();
        searchInput_PlaceholderColor_inst = new getAFuse_FuseControlsTextInput_PlaceholderColor_Property(searchInput, __selector4);
        searchCloseButton = new global::Fuse.Controls.Rectangle();
        searchCloseButton_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(searchCloseButton, __selector0);
        searchInput_Value_inst = new getAFuse_FuseControlsTextInputControl_Value_Property(searchInput, __selector5);
        searchFieldActive = new global::Fuse.Triggers.WhileTrue();
        searchFieldActive_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(searchFieldActive, __selector5);
        var temp1 = new global::Fuse.Reactive.Name("searchInput");
        var temp = new global::Fuse.Triggers.WhileString();
        temp_Value_inst = new getAFuse_FuseTriggersWhileString_Value_Property(temp, __selector5);
        var temp2 = new global::Fuse.Reactive.Property(temp1, getAFuse_accessor_Fuse_Controls_TextInputControl_Value.Singleton);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        menuIsOpen = new global::Fuse.Triggers.WhileTrue();
        menuIsOpen_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(menuIsOpen, __selector5);
        var temp3 = new global::Fuse.iOS.StatusBarConfig();
        var temp4 = new global::Fuse.Elements.Viewport();
        var temp5 = new global::Fuse.Controls.DockPanel();
        var temp6 = new global::Fuse.Controls.StatusBarBackground();
        var temp7 = new global::Fuse.Animations.Rotate();
        var temp8 = new global::Fuse.Animations.Change<float>(content_Opacity_inst);
        var temp9 = new global::Fuse.Animations.Change<float>(menu_Opacity_inst);
        var temp10 = new global::Fuse.Animations.Change<bool>(hamburger_IsOpen_inst);
        var temp11 = new global::Fuse.Controls.Panel();
        var temp12 = new global::Fuse.Gestures.Clicked();
        var temp13 = new global::Fuse.Triggers.Actions.Toggle();
        box = new global::Fuse.Controls.Panel();
        sidePage = new global::Fuse.Controls.Panel();
        var temp14 = new global::Fuse.Rotation();
        var temp15 = new global::Fuse.Animations.Change<Fuse.Elements.Element>(searchField_Element_LayoutMaster_inst);
        var temp16 = new global::Fuse.Animations.Change<double>(transitionToSearchLayout_TargetProgress_inst);
        var temp17 = new global::Fuse.Animations.Move();
        var temp18 = new global::Fuse.Animations.Change<float>(normalMenuItems_Opacity_inst);
        var temp19 = new global::Fuse.Animations.Move();
        var temp20 = new global::Fuse.Animations.Change<float4>(searchInput_PlaceholderColor_inst);
        var temp21 = new global::Fuse.Animations.Change<float>(searchCloseButton_Opacity_inst);
        searchLayout = new global::Fuse.Controls.DockPanel();
        searchFieldLayoutMaster = new global::Fuse.Controls.Panel();
        var temp22 = new global::Fuse.Gestures.Clicked();
        var temp23 = new global::Fuse.Triggers.Actions.Set<string>(searchInput_Value_inst);
        var temp24 = new global::Fuse.Controls.Rectangle();
        var temp25 = new global::Fuse.Rotation();
        var temp26 = new global::Fuse.Controls.Rectangle();
        var temp27 = new global::Fuse.Rotation();
        var temp28 = new global::Fuse.Controls.Panel();
        var temp29 = new global::Fuse.Triggers.WhileFocused();
        var temp30 = new global::Fuse.Animations.Change<bool>(searchFieldActive_Value_inst);
        var temp31 = new global::Fuse.Animations.Change<bool>(searchFieldActive_Value_inst);
        var temp32 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Read);
        var temp33 = new global::Fuse.Triggers.LayoutAnimation();
        var temp34 = new global::Fuse.Animations.Resize();
        var temp35 = new global::Fuse.Animations.Move();
        var temp36 = new global::MenuButton();
        var temp37 = new global::Fuse.Gestures.Clicked();
        var temp38 = new global::Fuse.Triggers.Actions.Set<bool>(menuIsOpen_Value_inst);
        var temp39 = new global::MenuButton();
        var temp40 = new global::Fuse.Gestures.Clicked();
        var temp41 = new global::Fuse.Triggers.Actions.Set<bool>(menuIsOpen_Value_inst);
        var temp42 = new global::MenuButton();
        var temp43 = new global::Fuse.Gestures.Clicked();
        var temp44 = new global::Fuse.Triggers.Actions.Set<bool>(menuIsOpen_Value_inst);
        var temp45 = new global::Fuse.Controls.Panel();
        var temp46 = new global::Fuse.Controls.Text();
        var temp47 = new global::Fuse.Controls.ScrollView();
        var temp48 = new global::Fuse.Controls.Grid();
        var temp49 = new global::Fuse.Reactive.Each();
        var temp50 = new Template(this, this);
        var temp51 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9764706f, 0.9764706f, 0.9764706f, 1f));
        var temp52 = new global::Fuse.Drawing.StaticSolidColor(float4(0.1294118f, 0.1294118f, 0.1294118f, 1f));
        temp3.Style = Fuse.Platform.StatusBarStyle.Light;
        temp4.CullFace = Uno.Graphics.PolygonFace.Back;
        temp4.Perspective = 1.7f;
        temp4.PerspectiveRelativeTo = Fuse.Elements.PerspectiveRelativeToMode.Width;
        temp4.Children.Add(temp5);
        temp5.Children.Add(temp6);
        temp5.Children.Add(menuIsOpen);
        temp5.Children.Add(temp11);
        global::Fuse.Controls.DockPanel.SetDock(temp6, Fuse.Layouts.Dock.Top);
        menuIsOpen.Value = false;
        menuIsOpen.Name = __selector6;
        menuIsOpen.Animators.Add(temp7);
        menuIsOpen.Animators.Add(temp8);
        menuIsOpen.Animators.Add(temp9);
        menuIsOpen.Animators.Add(temp10);
        temp7.DegreesY = -90f;
        temp7.Duration = 0.7;
        temp7.DelayBack = 0;
        temp7.Delay = 0;
        temp7.Target = box;
        temp7.Easing = Fuse.Animations.Easing.ExponentialOut;
        temp7.EasingBack = Fuse.Animations.Easing.ExponentialIn;
        temp8.Value = 0f;
        temp8.Duration = 0.7;
        temp8.DelayBack = 0;
        temp8.Easing = Fuse.Animations.Easing.ExponentialOut;
        temp8.EasingBack = Fuse.Animations.Easing.ExponentialIn;
        temp9.Value = 1f;
        temp9.Duration = 0.3;
        temp9.DelayBack = 0;
        temp9.Easing = Fuse.Animations.Easing.QuarticIn;
        temp9.EasingBack = Fuse.Animations.Easing.QuarticIn;
        temp10.Value = true;
        temp10.DelayBack = 0;
        temp11.Children.Add(hamburger);
        temp11.Children.Add(box);
        hamburger.Alignment = Fuse.Elements.Alignment.TopLeft;
        hamburger.Name = __selector7;
        hamburger.Children.Add(temp12);
        temp12.Actions.Add(temp13);
        temp13.Target = menuIsOpen;
        box.Name = __selector8;
        box.TransformOrigin = Fuse.Elements.TransformOrigins.VerticalBoxCenter;
        box.Children.Add(sidePage);
        box.Children.Add(content);
        sidePage.Name = __selector9;
        sidePage.TransformOrigin = Fuse.Elements.TransformOrigins.VerticalBoxCenter;
        sidePage.Children.Add(temp14);
        sidePage.Children.Add(searchFieldActive);
        sidePage.Children.Add(transitionToSearchLayout);
        sidePage.Children.Add(searchLayout);
        sidePage.Children.Add(menu);
        temp14.DegreesY = 90f;
        searchFieldActive.Value = false;
        searchFieldActive.Name = __selector10;
        searchFieldActive.Animators.Add(temp15);
        searchFieldActive.Animators.Add(temp16);
        temp15.Value = searchFieldLayoutMaster;
        temp15.DelayBack = 0.07;
        temp15.Delay = 0.07;
        temp16.Value = 1;
        transitionToSearchLayout.Name = __selector11;
        transitionToSearchLayout.Animators.Add(temp17);
        transitionToSearchLayout.Animators.Add(temp18);
        transitionToSearchLayout.Animators.Add(temp19);
        transitionToSearchLayout.Animators.Add(temp20);
        transitionToSearchLayout.Animators.Add(temp21);
        temp17.Y = 1f;
        temp17.Duration = 0.5;
        temp17.Delay = 0.1;
        temp17.RelativeTo = Fuse.TranslationModes.Size;
        temp17.Target = normalMenuItems;
        temp17.Easing = Fuse.Animations.Easing.CubicInOut;
        temp18.Value = 0f;
        temp18.Duration = 0.3;
        temp18.Delay = 0.1;
        temp18.Easing = Fuse.Animations.Easing.CubicInOut;
        temp19.Y = -2f;
        temp19.Duration = 0.4;
        temp19.RelativeTo = Fuse.TranslationModes.Size;
        temp19.Target = hamburger;
        temp19.Easing = Fuse.Animations.Easing.CubicIn;
        temp20.Value = float4(0.8f, 0.8f, 0.8f, 0.4f);
        temp20.Duration = 0.5;
        temp20.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp21.Value = 1f;
        temp21.Duration = 0.2;
        temp21.DelayBack = 0;
        temp21.Easing = Fuse.Animations.Easing.QuadraticInOut;
        searchLayout.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        searchLayout.Alignment = Fuse.Elements.Alignment.Top;
        searchLayout.Margin = float4(30f, 30f, 30f, 30f);
        searchLayout.Name = __selector12;
        searchLayout.Children.Add(searchFieldLayoutMaster);
        searchLayout.Children.Add(searchCloseButton);
        searchFieldLayoutMaster.Name = __selector13;
        searchCloseButton.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        searchCloseButton.Margin = float4(15f, 0f, 0f, 0f);
        searchCloseButton.Opacity = 0f;
        searchCloseButton.Name = __selector14;
        global::Fuse.Controls.DockPanel.SetDock(searchCloseButton, Fuse.Layouts.Dock.Right);
        searchCloseButton.Children.Add(temp22);
        searchCloseButton.Children.Add(temp24);
        searchCloseButton.Children.Add(temp26);
        temp22.Actions.Add(temp23);
        temp23.Value = "";
        temp24.CornerRadius = float4(3f, 3f, 3f, 3f);
        temp24.Color = float4(0.7333333f, 0.7333333f, 0.7333333f, 1f);
        temp24.Width = new Uno.UX.Size(15f, Uno.UX.Unit.Unspecified);
        temp24.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        temp24.Children.Add(temp25);
        temp25.Degrees = 45f;
        temp26.CornerRadius = float4(3f, 3f, 3f, 3f);
        temp26.Color = float4(0.7333333f, 0.7333333f, 0.7333333f, 1f);
        temp26.Width = new Uno.UX.Size(15f, Uno.UX.Unit.Unspecified);
        temp26.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        temp26.Children.Add(temp27);
        temp27.Degrees = -45f;
        menu.ItemSpacing = 50f;
        menu.ContentAlignment = Fuse.Elements.Alignment.VerticalCenter;
        menu.Margin = float4(70f, 0f, 0f, 0f);
        menu.Opacity = 0f;
        menu.Name = __selector15;
        menu.Children.Add(temp28);
        menu.Children.Add(normalMenuItems);
        temp28.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp28.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp28.Children.Add(searchField);
        searchField.Name = __selector16;
        searchField.Children.Add(searchInput);
        searchField.Children.Add(temp33);
        searchInput.PlaceholderText = "search";
        searchInput.PlaceholderColor = float4(0.9254902f, 0.9372549f, 0.945098f, 1f);
        searchInput.FontSize = 28f;
        searchInput.TextColor = float4(0.9254902f, 0.9372549f, 0.945098f, 1f);
        searchInput.CaretColor = float4(1f, 1f, 1f, 0.6666667f);
        searchInput.Alignment = Fuse.Elements.Alignment.Top;
        searchInput.Name = __selector17;
        searchInput.Children.Add(temp29);
        searchInput.Children.Add(temp);
        temp29.Animators.Add(temp30);
        temp30.Value = true;
        temp.Test = Fuse.Triggers.WhileStringTest.IsNotEmpty;
        temp.Animators.Add(temp31);
        temp.Bindings.Add(temp32);
        temp31.Value = true;
        temp33.Animators.Add(temp34);
        temp33.Animators.Add(temp35);
        temp34.X = 1f;
        temp34.Y = 1f;
        temp34.DurationBack = 0.3;
        temp34.DelayBack = 0.2;
        temp34.RelativeTo = Fuse.Triggers.LayoutTransition.ResizeSizeChange;
        temp34.Easing = Fuse.Animations.Easing.ExponentialInOut;
        temp35.X = 1f;
        temp35.Y = 1f;
        temp35.Duration = 0.3;
        temp35.DelayBack = 0;
        temp35.RelativeTo = Fuse.Triggers.LayoutTransition.WorldPositionChange;
        temp35.Easing = Fuse.Animations.Easing.QuarticIn;
        normalMenuItems.ItemSpacing = 50f;
        normalMenuItems.Name = __selector18;
        normalMenuItems.Children.Add(temp36);
        normalMenuItems.Children.Add(temp39);
        normalMenuItems.Children.Add(temp42);
        temp36.Alignment = Fuse.Elements.Alignment.Left;
        temp36.Text = "home";
        temp36.Children.Add(temp37);
        temp37.Actions.Add(temp38);
        temp38.Value = false;
        temp39.Alignment = Fuse.Elements.Alignment.Left;
        temp39.Text = "discover";
        temp39.Children.Add(temp40);
        temp40.Actions.Add(temp41);
        temp41.Value = false;
        temp42.Alignment = Fuse.Elements.Alignment.Left;
        temp42.Text = "settings";
        temp42.Children.Add(temp43);
        temp43.Actions.Add(temp44);
        temp44.Value = false;
        content.Name = __selector19;
        content.Children.Add(temp45);
        content.Children.Add(temp47);
        temp45.Height = new Uno.UX.Size(56f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.DockPanel.SetDock(temp45, Fuse.Layouts.Dock.Top);
        temp45.Children.Add(temp46);
        temp46.Value = "boxify";
        temp46.FontSize = 21f;
        temp46.Color = float4(0.9254902f, 0.9372549f, 0.945098f, 1f);
        temp46.Alignment = Fuse.Elements.Alignment.Center;
        temp47.Background = temp51;
        temp47.Children.Add(temp48);
        temp48.ColumnCount = 4;
        temp48.DefaultColumn = "1*";
        temp48.CellSpacing = 10f;
        temp48.Margin = float4(10f, 10f, 10f, 10f);
        temp48.Children.Add(temp49);
        temp49.Count = 23;
        temp49.Templates.Add(temp50);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(menuIsOpen);
        __g_nametable.Objects.Add(hamburger);
        __g_nametable.Objects.Add(box);
        __g_nametable.Objects.Add(sidePage);
        __g_nametable.Objects.Add(searchFieldActive);
        __g_nametable.Objects.Add(transitionToSearchLayout);
        __g_nametable.Objects.Add(searchLayout);
        __g_nametable.Objects.Add(searchFieldLayoutMaster);
        __g_nametable.Objects.Add(searchCloseButton);
        __g_nametable.Objects.Add(menu);
        __g_nametable.Objects.Add(searchField);
        __g_nametable.Objects.Add(searchInput);
        __g_nametable.Objects.Add(normalMenuItems);
        __g_nametable.Objects.Add(content);
        this.Background = temp52;
        this.Children.Add(temp3);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Opacity";
    static global::Uno.UX.Selector __selector1 = "IsOpen";
    static global::Uno.UX.Selector __selector2 = "Element.LayoutMaster";
    static global::Uno.UX.Selector __selector3 = "TargetProgress";
    static global::Uno.UX.Selector __selector4 = "PlaceholderColor";
    static global::Uno.UX.Selector __selector5 = "Value";
    static global::Uno.UX.Selector __selector6 = "menuIsOpen";
    static global::Uno.UX.Selector __selector7 = "hamburger";
    static global::Uno.UX.Selector __selector8 = "box";
    static global::Uno.UX.Selector __selector9 = "sidePage";
    static global::Uno.UX.Selector __selector10 = "searchFieldActive";
    static global::Uno.UX.Selector __selector11 = "transitionToSearchLayout";
    static global::Uno.UX.Selector __selector12 = "searchLayout";
    static global::Uno.UX.Selector __selector13 = "searchFieldLayoutMaster";
    static global::Uno.UX.Selector __selector14 = "searchCloseButton";
    static global::Uno.UX.Selector __selector15 = "menu";
    static global::Uno.UX.Selector __selector16 = "searchField";
    static global::Uno.UX.Selector __selector17 = "searchInput";
    static global::Uno.UX.Selector __selector18 = "normalMenuItems";
    static global::Uno.UX.Selector __selector19 = "content";
}
