[Uno.Compiler.UxGenerated]
public partial class Divider: Fuse.Controls.Panel
{
    static Divider()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Divider()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Color = float4(0.8705882f, 0.9215686f, 0.9568627f, 1f);
        this.Height = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(30f, 15f, 30f, 15f);
    }
}
