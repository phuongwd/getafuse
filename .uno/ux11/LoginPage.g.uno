[Uno.Compiler.UxGenerated]
public partial class LoginPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<string> temp2_Text_inst;
    internal global::Fuse.iOS.StatusBarConfig statusBarConfig;
    internal global::Fuse.Reactive.EventBinding temp_eb21;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "statusBarConfig",
        "temp_eb21"
    };
    static LoginPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public LoginPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::MyTextInput();
        temp_Value_inst = new getAFuse_FuseControlsTextInputControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("username");
        var temp1 = new global::MyTextInput();
        temp1_Value_inst = new getAFuse_FuseControlsTextInputControl_Value_Property(temp1, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("password");
        var temp2 = new global::MyButton();
        temp2_Text_inst = new getAFuse_MyButton_Text_Property(temp2, __selector1);
        var temp5 = new global::Fuse.Reactive.Data("textLogin");
        var temp6 = new global::Fuse.Reactive.Data("login");
        var temp7 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        statusBarConfig = new global::Fuse.iOS.StatusBarConfig();
        var temp8 = new global::Fuse.Controls.TopFrameBackground();
        var temp9 = new global::Fuse.Controls.BottomFrameBackground();
        var temp10 = new global::Fuse.Controls.ScrollView();
        var temp11 = new global::Fuse.Controls.StackPanel();
        var temp12 = new global::Fuse.Controls.Text();
        var temp13 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp14 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp4, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp15 = new global::Fuse.Reactive.DataBinding(temp2_Text_inst, temp5, __g_nametable, Fuse.Reactive.BindingMode.Default);
        temp_eb21 = new global::Fuse.Reactive.EventBinding(temp6, __g_nametable);
        temp7.Code = "\n        var Observable = require(\"FuseJS/Observable\");\n\n        var username = Observable();\n        var password = Observable();\n        var textLogin = Observable('Login');\n\n        var ROOT_URL = \"https://sta.getabstract.com/\";\n        var apiDefaultPath = 'api/v7/mobile/';\n        var apiOauthTokenPath = 'api/oauth/token';\n\n        function login() {\n\n            var status = 0;\n            var response_ok = false;\n\n            var params = 'grant_type=password&username=' + encodeURIComponent(username.value) + '&password=' + encodeURIComponent(password.value);\n            var myRequest = new Request('https://www.getabstract.com/api/oauth/token', {\n                method: 'POST', \n                headers: {\n                    \"Authorization\": \"Basic Z2V0YWJzdHJhY3QtbW9iaWxlLW5hdGl2ZS1hcHA6c1lNUFRKcG5JcHJER1FBaA==\",\n                    'Accept': 'application/json, text/javascript, */*; q=0.01',\n                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',\n                    \"Device-Id\": \"CC3AC87A-C4B5-4806-97EF-34534543\",\n                    \"Application\":  \"APP_PHONE\",\n                    \"OS\": \"IOS\",\n                    \"Version\":  \"7.9.0\"\n                },\n                body: params\n            });\n            \n            textLogin.value = 'Logging...';\n            fetch(myRequest).then(function(response) {\n                status = response.status;  // Get the HTTP status code\n                response_ok = response.ok; // Is response.status in the 200-range?\n                console.log('response');\n                console.log(JSON.stringify(response));\n                \n                return response.json();    // This returns a promise\n            }).then(function(responseObject) {\n                console.log('responseObject');\n                console.log(JSON.stringify(responseObject));\n                if(responseObject.error) {\n                    textLogin.value = 'Login';\n                } else {\n                    localStorage.setItem('gaTokens', JSON.stringify(responseObject));\n                    textLogin.value = 'Login';\n                    router.push('home');\n                }\n                // Do something with the result\n            }).catch(function(err) {\n                console.log('errrorrr');\n                conosole.log(err);\n                // An error occurred somewhere in the Promise chain\n            });\n        }\n\n        module.exports = {\n            username: username,\n            password: password,\n\n            login: login,\n            textLogin: textLogin\n        };\n    ";
        temp7.LineNumber = 3;
        temp7.FileName = "Pages/Login/LoginPage.ux";
        statusBarConfig.Style = Fuse.Platform.StatusBarStyle.Dark;
        statusBarConfig.Name = __selector2;
        global::Fuse.Controls.DockPanel.SetDock(temp8, Fuse.Layouts.Dock.Top);
        global::Fuse.Controls.DockPanel.SetDock(temp9, Fuse.Layouts.Dock.Bottom);
        temp10.Children.Add(temp11);
        temp11.ItemSpacing = 10f;
        temp11.Margin = float4(10f, 10f, 10f, 10f);
        temp11.Padding = float4(0f, 50f, 0f, 0f);
        temp11.Children.Add(temp12);
        temp11.Children.Add(temp);
        temp11.Children.Add(temp1);
        temp11.Children.Add(temp2);
        temp12.Value = "Login";
        temp12.FontSize = 30f;
        temp.PlaceholderText = "Username";
        temp.Bindings.Add(temp13);
        temp1.IsPassword = true;
        temp1.PlaceholderText = "Password";
        temp1.ActionStyle = Fuse.Controls.TextInputActionStyle.Send;
        temp1.Bindings.Add(temp14);
        temp2.Margin = float4(20f, 20f, 20f, 20f);
        global::Fuse.Gestures.Clicked.AddHandler(temp2, temp_eb21.OnEvent);
        temp2.Bindings.Add(temp15);
        temp2.Bindings.Add(temp_eb21);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(statusBarConfig);
        __g_nametable.Objects.Add(temp_eb21);
        this.Children.Add(temp7);
        this.Children.Add(statusBarConfig);
        this.Children.Add(temp8);
        this.Children.Add(temp9);
        this.Children.Add(temp10);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Text";
    static global::Uno.UX.Selector __selector2 = "statusBarConfig";
}
