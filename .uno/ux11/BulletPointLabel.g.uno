[Uno.Compiler.UxGenerated]
public partial class BulletPointLabel: Fuse.Controls.Panel
{
    string _field_Text;
    [global::Uno.UX.UXOriginSetter("SetText")]
    public string Text
    {
        get { return _field_Text; }
        set { SetText(value, null); }
    }
    public void SetText(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Text)
        {
            _field_Text = value;
            OnPropertyChanged("Text", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> this_Text_inst;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
    };
    static BulletPointLabel()
    {
    }
    [global::Uno.UX.UXConstructor]
    public BulletPointLabel()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.This();
        var temp = new global::StyledLabel();
        temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, getAFuse_accessor_BulletPointLabel_Text.Singleton);
        this_Text_inst = new getAFuse_BulletPointLabel_Text_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp3 = new global::Fuse.Controls.Circle();
        var temp4 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Read);
        this.Margin = float4(0f, 0f, 0f, 0f);
        temp3.Color = float4(0.6705883f, 0.7098039f, 0.7372549f, 1f);
        temp3.Width = new Uno.UX.Size(10f, Uno.UX.Unit.Unspecified);
        temp3.Height = new Uno.UX.Size(10f, Uno.UX.Unit.Unspecified);
        temp3.Alignment = Fuse.Elements.Alignment.Left;
        temp3.Margin = float4(10f, 5f, 5f, 5f);
        temp.Alignment = Fuse.Elements.Alignment.Left;
        temp.Margin = float4(30f, 0f, 0f, 0f);
        temp.Bindings.Add(temp4);
        __g_nametable.This = this;
        __g_nametable.Properties.Add(this_Text_inst);
        this.Children.Add(temp3);
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Text";
}
