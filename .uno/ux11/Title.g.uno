[Uno.Compiler.UxGenerated]
public partial class Title: Fuse.Controls.Text
{
    static Title()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Title()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Shadow();
        this.FontSize = 30f;
        this.Color = float4(1f, 1f, 1f, 1f);
        this.HitTestMode = Fuse.Elements.HitTestMode.None;
        this.Alignment = Fuse.Elements.Alignment.TopLeft;
        this.Margin = float4(10f, 10f, 10f, 10f);
        temp.Mode = Fuse.Controls.Shadow.ShadowMode.PerPixel;
        temp.Distance = 1f;
        temp.Size = 10f;
        temp.Color = float4(0f, 0f, 0f, 1f);
        this.Children.Add(temp);
    }
}
