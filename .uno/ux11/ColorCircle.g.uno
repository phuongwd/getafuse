[Uno.Compiler.UxGenerated]
public partial class ColorCircle: Fuse.Controls.Circle
{
    static ColorCircle()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ColorCircle()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        this.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(13f, 13f, 13f, 13f);
    }
}
