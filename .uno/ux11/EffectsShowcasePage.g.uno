[Uno.Compiler.UxGenerated]
public partial class EffectsShowcasePage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<float> blur_Radius_inst;
    global::Uno.UX.Property<float> halftone_DotTint_inst;
    global::Uno.UX.Property<float> halftone_PaperTint_inst;
    global::Uno.UX.Property<float> halftone_Intensity_inst;
    global::Uno.UX.Property<float> desaturate_Amount_inst;
    global::Uno.UX.Property<float4> maskBackground_Color_inst;
    internal global::Fuse.Effects.Blur blur;
    internal global::Fuse.Effects.Halftone halftone;
    internal global::Fuse.Effects.Desaturate desaturate;
    internal global::MyImage maskBackground;
    static EffectsShowcasePage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public EffectsShowcasePage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        blur = new global::Fuse.Effects.Blur();
        blur_Radius_inst = new getAFuse_FuseEffectsBlur_Radius_Property(blur, __selector0);
        halftone = new global::Fuse.Effects.Halftone();
        halftone_DotTint_inst = new getAFuse_FuseEffectsHalftone_DotTint_Property(halftone, __selector1);
        halftone_PaperTint_inst = new getAFuse_FuseEffectsHalftone_PaperTint_Property(halftone, __selector2);
        halftone_Intensity_inst = new getAFuse_FuseEffectsHalftone_Intensity_Property(halftone, __selector3);
        desaturate = new global::Fuse.Effects.Desaturate();
        desaturate_Amount_inst = new getAFuse_FuseEffectsDesaturate_Amount_Property(desaturate, __selector4);
        maskBackground = new global::MyImage();
        maskBackground_Color_inst = new getAFuse_FuseControlsImage_Color_Property(maskBackground, __selector5);
        var temp = new global::Fuse.Controls.Grid();
        var temp1 = new global::Fuse.Controls.Panel();
        var temp2 = new global::Fuse.Controls.Panel();
        var temp3 = new global::Fuse.Controls.StackPanel();
        var temp4 = new global::Fuse.Controls.StatusBarBackground();
        var temp5 = new global::Title();
        var temp6 = new global::MyImage();
        var temp7 = new global::Fuse.Gestures.WhilePressed();
        var temp8 = new global::Fuse.Animations.Change<float>(blur_Radius_inst);
        var temp9 = new global::Fuse.Controls.Panel();
        var temp10 = new global::MyImage();
        var temp11 = new global::Title();
        var temp12 = new global::Fuse.Gestures.WhilePressed();
        var temp13 = new global::Fuse.Animations.Change<float>(halftone_DotTint_inst);
        var temp14 = new global::Fuse.Animations.Change<float>(halftone_PaperTint_inst);
        var temp15 = new global::Fuse.Animations.Change<float>(halftone_Intensity_inst);
        var temp16 = new global::Fuse.Controls.Panel();
        var temp17 = new global::Title();
        var temp18 = new global::MyImage();
        var temp19 = new global::Fuse.Gestures.WhilePressed();
        var temp20 = new global::Fuse.Animations.Change<float>(desaturate_Amount_inst);
        var temp21 = new global::Fuse.Controls.Panel();
        var temp22 = new global::Fuse.Controls.Text();
        var temp23 = new global::Fuse.Effects.Mask();
        var temp24 = new global::Fuse.Gestures.WhilePressed();
        var temp25 = new global::Fuse.Animations.Change<float4>(maskBackground_Color_inst);
        temp.RowCount = 4;
        temp.DefaultRow = "1*";
        temp.Children.Add(temp1);
        temp.Children.Add(temp9);
        temp.Children.Add(temp16);
        temp.Children.Add(temp21);
        temp1.ClipToBounds = true;
        temp1.Children.Add(temp2);
        temp1.Children.Add(temp7);
        temp2.Children.Add(temp3);
        temp2.Children.Add(temp6);
        temp2.Children.Add(blur);
        temp3.Children.Add(temp4);
        temp3.Children.Add(temp5);
        temp5.Value = "BLUR";
        temp6.Margin = float4(-20f, -20f, -20f, -20f);
        temp6.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/blur.png"));
        blur.Radius = 0f;
        blur.Name = __selector6;
        temp7.Animators.Add(temp8);
        temp8.Value = 10f;
        temp8.Duration = 0.3;
        temp9.Children.Add(temp10);
        temp9.Children.Add(temp12);
        temp10.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/halftone.png"));
        temp10.Children.Add(temp11);
        temp10.Children.Add(halftone);
        temp11.Value = "HALFTONE";
        halftone.Spacing = 5f;
        halftone.Intensity = 0.2f;
        halftone.Smoothness = 1f;
        halftone.DotTint = 1f;
        halftone.PaperTint = 1f;
        halftone.Name = __selector7;
        temp12.Animators.Add(temp13);
        temp12.Animators.Add(temp14);
        temp12.Animators.Add(temp15);
        temp13.Value = 0.1f;
        temp13.Duration = 0.3;
        temp14.Value = 0.5f;
        temp14.Duration = 0.3;
        temp15.Value = 0.6f;
        temp15.Duration = 0.3;
        temp16.Children.Add(temp17);
        temp16.Children.Add(temp18);
        temp17.Value = "DESATURATE";
        temp18.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/desaturate.png"));
        temp18.Children.Add(desaturate);
        temp18.Children.Add(temp19);
        desaturate.Amount = 0f;
        desaturate.Name = __selector8;
        temp19.Animators.Add(temp20);
        temp20.Value = 1f;
        temp20.Duration = 0.3;
        temp20.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp21.Children.Add(temp22);
        temp21.Children.Add(maskBackground);
        temp21.Children.Add(temp24);
        temp22.Value = "MASK";
        temp22.FontSize = 130f;
        temp22.Color = float4(1f, 1f, 1f, 1f);
        temp22.Alignment = Fuse.Elements.Alignment.Center;
        temp22.Children.Add(temp23);
        temp23.Mode = Fuse.Effects.Mask.MaskMode.Grayscale;
        temp23.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/mask.png"));
        maskBackground.Name = __selector9;
        maskBackground.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/mask_bg.png"));
        temp24.Animators.Add(temp25);
        temp25.Value = float4(0.06666667f, 0.06666667f, 0.06666667f, 1f);
        temp25.Duration = 0.3;
        temp25.Easing = Fuse.Animations.Easing.QuadraticInOut;
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "Radius";
    static global::Uno.UX.Selector __selector1 = "DotTint";
    static global::Uno.UX.Selector __selector2 = "PaperTint";
    static global::Uno.UX.Selector __selector3 = "Intensity";
    static global::Uno.UX.Selector __selector4 = "Amount";
    static global::Uno.UX.Selector __selector5 = "Color";
    static global::Uno.UX.Selector __selector6 = "blur";
    static global::Uno.UX.Selector __selector7 = "halftone";
    static global::Uno.UX.Selector __selector8 = "desaturate";
    static global::Uno.UX.Selector __selector9 = "maskBackground";
}
