[Uno.Compiler.UxGenerated]
public partial class FuseExamplesPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    internal global::Fuse.Reactive.EventBinding temp_eb10;
    internal global::Fuse.Reactive.EventBinding temp_eb11;
    internal global::Fuse.Reactive.EventBinding temp_eb12;
    internal global::Fuse.Reactive.EventBinding temp_eb13;
    internal global::Fuse.Reactive.EventBinding temp_eb14;
    internal global::Fuse.Reactive.EventBinding temp_eb15;
    internal global::Fuse.Reactive.EventBinding temp_eb16;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb10",
        "temp_eb11",
        "temp_eb12",
        "temp_eb13",
        "temp_eb14",
        "temp_eb15",
        "temp_eb16"
    };
    static FuseExamplesPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public FuseExamplesPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Data("goToCubeTransition");
        var temp1 = new global::Fuse.Reactive.Data("goToInteractiveInfographic");
        var temp2 = new global::Fuse.Reactive.Data("goToExploring");
        var temp3 = new global::Fuse.Reactive.Data("goToEffectsShowcase");
        var temp4 = new global::Fuse.Reactive.Data("goToRotatingPages");
        var temp5 = new global::Fuse.Reactive.Data("goToDynamicTabBar");
        var temp6 = new global::Fuse.Reactive.Data("goToCircleMenu");
        var temp7 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp8 = new global::Fuse.Controls.Grid();
        var temp9 = new global::Fuse.Controls.Panel();
        var temp10 = new global::Fuse.Controls.StatusBarBackground();
        var temp11 = new global::Fuse.Controls.Text();
        var temp12 = new global::Fuse.Controls.Panel();
        var temp13 = new global::Fuse.Controls.ScrollView();
        var temp14 = new global::Fuse.Controls.StackPanel();
        var temp15 = new global::Separator();
        var temp16 = new global::Fuse.Controls.Panel();
        var temp17 = new global::Fuse.Controls.Text();
        temp_eb10 = new global::Fuse.Reactive.EventBinding(temp, __g_nametable);
        var temp18 = new global::Separator();
        var temp19 = new global::Fuse.Controls.Panel();
        var temp20 = new global::Fuse.Controls.Text();
        temp_eb11 = new global::Fuse.Reactive.EventBinding(temp1, __g_nametable);
        var temp21 = new global::Separator();
        var temp22 = new global::Fuse.Controls.Panel();
        var temp23 = new global::Fuse.Controls.Text();
        temp_eb12 = new global::Fuse.Reactive.EventBinding(temp2, __g_nametable);
        var temp24 = new global::Separator();
        var temp25 = new global::Fuse.Controls.Panel();
        var temp26 = new global::Fuse.Controls.Text();
        temp_eb13 = new global::Fuse.Reactive.EventBinding(temp3, __g_nametable);
        var temp27 = new global::Separator();
        var temp28 = new global::Fuse.Controls.Panel();
        var temp29 = new global::Fuse.Controls.Text();
        temp_eb14 = new global::Fuse.Reactive.EventBinding(temp4, __g_nametable);
        var temp30 = new global::Separator();
        var temp31 = new global::Fuse.Controls.Panel();
        var temp32 = new global::Fuse.Controls.Text();
        temp_eb15 = new global::Fuse.Reactive.EventBinding(temp5, __g_nametable);
        var temp33 = new global::Separator();
        var temp34 = new global::Fuse.Controls.Panel();
        var temp35 = new global::Fuse.Controls.Text();
        temp_eb16 = new global::Fuse.Reactive.EventBinding(temp6, __g_nametable);
        var temp36 = new global::Separator();
        temp7.Code = "\n        function goToExploring() {\n            router.push('exploring');\n        }\n        function goToCubeTransition() {\n            router.push('cube');\n        }\n        function goToEffectsShowcase() {\n            router.push('effectshowcase');\n        }\n        function goToRotatingPages() {\n            router.push('rotatingPages');\n        }\n        function goToDynamicTabBar() {\n            router.push('dynamicTabBar');\n        }\n        function goToInteractiveInfographic() {\n            router.push('InteractiveInfographic');\n        }\n        function goToCircleMenu() {\n            router.push('CircleMenu');\n        }\n        module.exports = {\n            goToExploring: goToExploring,\n            goToCubeTransition: goToCubeTransition,\n            goToEffectsShowcase: goToEffectsShowcase,\n            goToRotatingPages: goToRotatingPages,\n            goToDynamicTabBar: goToDynamicTabBar,\n            goToInteractiveInfographic: goToInteractiveInfographic,\n            goToCircleMenu: goToCircleMenu\n        }\n    ";
        temp7.LineNumber = 3;
        temp7.FileName = "Pages/FuseExamples/FuseExamplesPage.ux";
        temp8.Rows = "auto,1*";
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp12);
        temp9.Color = float4(1f, 1f, 1f, 1f);
        temp9.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.Grid.SetRow(temp9, 0);
        temp9.Children.Add(temp10);
        temp9.Children.Add(temp11);
        temp11.Value = "Fuse Examples";
        temp11.FontSize = 40f;
        temp11.Alignment = Fuse.Elements.Alignment.Center;
        global::Fuse.Controls.Grid.SetRow(temp12, 1);
        temp12.Children.Add(temp13);
        temp13.ClipToBounds = true;
        temp13.Children.Add(temp14);
        temp14.Children.Add(temp15);
        temp14.Children.Add(temp16);
        temp14.Children.Add(temp18);
        temp14.Children.Add(temp19);
        temp14.Children.Add(temp21);
        temp14.Children.Add(temp22);
        temp14.Children.Add(temp24);
        temp14.Children.Add(temp25);
        temp14.Children.Add(temp27);
        temp14.Children.Add(temp28);
        temp14.Children.Add(temp30);
        temp14.Children.Add(temp31);
        temp14.Children.Add(temp33);
        temp14.Children.Add(temp34);
        temp14.Children.Add(temp36);
        temp16.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp16, temp_eb10.OnEvent);
        temp16.Children.Add(temp17);
        temp16.Bindings.Add(temp_eb10);
        temp17.Value = "Cube Transition";
        temp17.Color = float4(0f, 0f, 0f, 1f);
        temp17.Margin = float4(15f, 15f, 15f, 15f);
        temp19.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp19, temp_eb11.OnEvent);
        temp19.Children.Add(temp20);
        temp19.Bindings.Add(temp_eb11);
        temp20.Value = "Interactive infographic";
        temp20.Color = float4(0f, 0f, 0f, 1f);
        temp20.Margin = float4(15f, 15f, 15f, 15f);
        temp22.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp22, temp_eb12.OnEvent);
        temp22.Children.Add(temp23);
        temp22.Bindings.Add(temp_eb12);
        temp23.Value = "Exploring";
        temp23.Color = float4(0f, 0f, 0f, 1f);
        temp23.Margin = float4(15f, 15f, 15f, 15f);
        temp25.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp25, temp_eb13.OnEvent);
        temp25.Children.Add(temp26);
        temp25.Bindings.Add(temp_eb13);
        temp26.Value = "Effects showcase - This example shows how you can apply various real-time visual effects to elements in your app. ";
        temp26.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp26.Color = float4(0f, 0f, 0f, 1f);
        temp26.Margin = float4(15f, 15f, 15f, 15f);
        temp28.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp28, temp_eb14.OnEvent);
        temp28.Children.Add(temp29);
        temp28.Bindings.Add(temp_eb14);
        temp29.Value = "Rotating pages";
        temp29.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp29.Color = float4(0f, 0f, 0f, 1f);
        temp29.Margin = float4(15f, 15f, 15f, 15f);
        temp31.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp31, temp_eb15.OnEvent);
        temp31.Children.Add(temp32);
        temp31.Bindings.Add(temp_eb15);
        temp32.Value = "Dynamic Tab Bar";
        temp32.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp32.Color = float4(0f, 0f, 0f, 1f);
        temp32.Margin = float4(15f, 15f, 15f, 15f);
        temp34.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        global::Fuse.Gestures.Clicked.AddHandler(temp34, temp_eb16.OnEvent);
        temp34.Children.Add(temp35);
        temp34.Bindings.Add(temp_eb16);
        temp35.Value = "Circle Menu";
        temp35.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp35.Color = float4(0f, 0f, 0f, 1f);
        temp35.Margin = float4(15f, 15f, 15f, 15f);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb10);
        __g_nametable.Objects.Add(temp_eb11);
        __g_nametable.Objects.Add(temp_eb12);
        __g_nametable.Objects.Add(temp_eb13);
        __g_nametable.Objects.Add(temp_eb14);
        __g_nametable.Objects.Add(temp_eb15);
        __g_nametable.Objects.Add(temp_eb16);
        this.Children.Add(temp7);
        this.Children.Add(temp8);
    }
}
