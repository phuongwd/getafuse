[Uno.Compiler.UxGenerated]
public partial class MyTextInput: Fuse.Controls.TextInput
{
    global::Uno.UX.Property<float4> txtStroke_Color_inst;
    global::Uno.UX.Property<float4> this_PlaceholderColor_inst;
    internal global::Fuse.Drawing.Stroke txtStroke;
    static MyTextInput()
    {
    }
    [global::Uno.UX.UXConstructor]
    public MyTextInput()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        txtStroke = new global::Fuse.Drawing.Stroke();
        txtStroke_Color_inst = new getAFuse_FuseDrawingStroke_Color_Property(txtStroke, __selector0);
        this_PlaceholderColor_inst = new getAFuse_FuseControlsTextInput_PlaceholderColor_Property(this, __selector1);
        var temp = new global::Fuse.Controls.Rectangle();
        var temp1 = new global::Fuse.Drawing.SolidColor();
        var temp2 = new global::Fuse.Triggers.WhileFocusWithin();
        var temp3 = new global::Fuse.Animations.Change<float4>(txtStroke_Color_inst);
        var temp4 = new global::Fuse.Animations.Change<float4>(this_PlaceholderColor_inst);
        this.PlaceholderColor = float4(0.8f, 0.8f, 0.8f, 1f);
        this.FontSize = 16f;
        this.Padding = float4(10f, 15f, 10f, 15f);
        temp.CornerRadius = float4(3f, 3f, 3f, 3f);
        temp.Layer = Fuse.Layer.Background;
        temp.Fills.Add(temp1);
        temp.Strokes.Add(txtStroke);
        txtStroke.Color = float4(0.8f, 0.8f, 0.8f, 1f);
        txtStroke.Width = 1f;
        temp1.Color = Fuse.Drawing.Colors.White;
        temp2.Animators.Add(temp3);
        temp2.Animators.Add(temp4);
        temp3.Value = float4(0.1333333f, 0.1333333f, 0.1333333f, 1f);
        temp3.Duration = 0.05;
        temp3.DurationBack = 0.2;
        temp4.Value = float4(0f, 0f, 0f, 1f);
        temp4.Duration = 0.05;
        temp4.DurationBack = 0.2;
        this.Children.Add(temp);
        this.Children.Add(temp2);
    }
    static global::Uno.UX.Selector __selector0 = "Color";
    static global::Uno.UX.Selector __selector1 = "PlaceholderColor";
}
