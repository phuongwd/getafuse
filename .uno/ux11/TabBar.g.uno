[Uno.Compiler.UxGenerated]
public partial class TabBar: Fuse.Controls.Panel
{
    global::Uno.UX.Property<Fuse.Elements.Element> indicator_Element_LayoutMaster_inst;
    internal global::Fuse.Controls.Rectangle indicator;
    internal global::Fuse.Controls.Panel page1Tab;
    internal global::Fuse.Reactive.EventBinding temp_eb0;
    internal global::Fuse.Controls.Panel page2Tab;
    internal global::Fuse.Reactive.EventBinding temp_eb1;
    internal global::Fuse.Controls.Panel page3Tab;
    internal global::Fuse.Reactive.EventBinding temp_eb2;
    internal global::Fuse.Controls.Panel page4Tab;
    internal global::Fuse.Reactive.EventBinding temp_eb3;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "indicator",
        "page1Tab",
        "temp_eb0",
        "page2Tab",
        "temp_eb1",
        "page3Tab",
        "temp_eb2",
        "page4Tab",
        "temp_eb3"
    };
    static TabBar()
    {
    }
    [global::Uno.UX.UXConstructor]
    public TabBar()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        indicator = new global::Fuse.Controls.Rectangle();
        indicator_Element_LayoutMaster_inst = new getAFuse_FuseElementsElement_ElementLayoutMaster_Property(indicator, __selector0);
        var temp = new global::Fuse.Reactive.Data("goHome");
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp1 = new global::Fuse.Reactive.Data("goExplore");
        var temp2 = new global::Fuse.Reactive.Data("goExamples");
        var temp3 = new global::Fuse.Reactive.Data("goWatch");
        var temp4 = new global::Fuse.Controls.Grid();
        var temp5 = new global::Fuse.Triggers.LayoutAnimation();
        var temp6 = new global::Fuse.Animations.Move();
        var temp7 = new global::Fuse.Controls.Rectangle();
        var temp8 = new global::Fuse.Drawing.Stroke();
        var temp9 = new global::Fuse.Controls.Grid();
        var temp10 = new global::Fuse.Controls.Rectangle();
        var temp11 = new global::Fuse.Drawing.Stroke();
        page1Tab = new global::Fuse.Controls.Panel();
        var temp12 = new global::Tab();
        var temp13 = new global::Fuse.Gestures.Clicked();
        var temp14 = new global::Fuse.Triggers.Actions.Set<Fuse.Elements.Element>(indicator_Element_LayoutMaster_inst);
        temp_eb0 = new global::Fuse.Reactive.EventBinding(temp, __g_nametable);
        page2Tab = new global::Fuse.Controls.Panel();
        var temp15 = new global::Tab();
        var temp16 = new global::Fuse.Gestures.Clicked();
        var temp17 = new global::Fuse.Triggers.Actions.Set<Fuse.Elements.Element>(indicator_Element_LayoutMaster_inst);
        temp_eb1 = new global::Fuse.Reactive.EventBinding(temp1, __g_nametable);
        page3Tab = new global::Fuse.Controls.Panel();
        var temp18 = new global::Tab();
        var temp19 = new global::Fuse.Gestures.Clicked();
        var temp20 = new global::Fuse.Triggers.Actions.Set<Fuse.Elements.Element>(indicator_Element_LayoutMaster_inst);
        temp_eb2 = new global::Fuse.Reactive.EventBinding(temp2, __g_nametable);
        page4Tab = new global::Fuse.Controls.Panel();
        var temp21 = new global::Tab();
        var temp22 = new global::Fuse.Gestures.Clicked();
        var temp23 = new global::Fuse.Triggers.Actions.Set<Fuse.Elements.Element>(indicator_Element_LayoutMaster_inst);
        temp_eb3 = new global::Fuse.Reactive.EventBinding(temp3, __g_nametable);
        var temp24 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        global::Fuse.Controls.DockPanel.SetDock(this, Fuse.Layouts.Dock.Bottom);
        temp4.Rows = "1, 49";
        temp4.ColumnCount = 1;
        temp4.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp4.Children.Add(indicator);
        temp4.Children.Add(temp7);
        temp4.Children.Add(temp9);
        indicator.Color = float4(0f, 0.682353f, 0.9372549f, 1f);
        indicator.Height = new Uno.UX.Size(4f, Uno.UX.Unit.Unspecified);
        indicator.Alignment = Fuse.Elements.Alignment.Bottom;
        indicator.Name = __selector1;
        global::Fuse.Controls.LayoutControl.SetLayoutMaster(indicator, page1Tab);
        indicator.Children.Add(temp5);
        temp5.Animators.Add(temp6);
        temp6.X = 1f;
        temp6.Duration = 0.4;
        temp6.RelativeTo = Fuse.Triggers.LayoutTransition.WorldPositionChange;
        temp6.Easing = Fuse.Animations.Easing.BackIn;
        temp7.Strokes.Add(temp8);
        temp8.Color = float4(0.8784314f, 0.8784314f, 0.8784314f, 1f);
        temp8.Width = 1f;
        temp9.ColumnCount = 4;
        temp9.Background = temp24;
        temp9.Children.Add(temp10);
        temp9.Children.Add(page1Tab);
        temp9.Children.Add(page2Tab);
        temp9.Children.Add(page3Tab);
        temp9.Children.Add(page4Tab);
        temp10.Alignment = Fuse.Elements.Alignment.Top;
        temp10.Layer = Fuse.Layer.Background;
        temp10.Strokes.Add(temp11);
        temp11.Color = float4(0f, 0f, 0f, 1f);
        temp11.Width = 100f;
        page1Tab.Name = __selector2;
        page1Tab.Children.Add(temp12);
        temp12.Text = "Today";
        temp12.Children.Add(temp13);
        temp13.Handler += temp_eb0.OnEvent;
        temp13.Actions.Add(temp14);
        temp13.Bindings.Add(temp_eb0);
        temp14.Value = page1Tab;
        page2Tab.Name = __selector3;
        page2Tab.Children.Add(temp15);
        temp15.Text = "Explore";
        temp15.Children.Add(temp16);
        temp16.Handler += temp_eb1.OnEvent;
        temp16.Actions.Add(temp17);
        temp16.Bindings.Add(temp_eb1);
        temp17.Value = page2Tab;
        page3Tab.Name = __selector4;
        page3Tab.Children.Add(temp18);
        temp18.Text = "Examples";
        temp18.Children.Add(temp19);
        temp19.Handler += temp_eb2.OnEvent;
        temp19.Actions.Add(temp20);
        temp19.Bindings.Add(temp_eb2);
        temp20.Value = page3Tab;
        page4Tab.Name = __selector5;
        page4Tab.Children.Add(temp21);
        temp21.Text = "Watch";
        temp21.Children.Add(temp22);
        temp22.Handler += temp_eb3.OnEvent;
        temp22.Actions.Add(temp23);
        temp22.Bindings.Add(temp_eb3);
        temp23.Value = page4Tab;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(indicator);
        __g_nametable.Objects.Add(page1Tab);
        __g_nametable.Objects.Add(temp_eb0);
        __g_nametable.Objects.Add(page2Tab);
        __g_nametable.Objects.Add(temp_eb1);
        __g_nametable.Objects.Add(page3Tab);
        __g_nametable.Objects.Add(temp_eb2);
        __g_nametable.Objects.Add(page4Tab);
        __g_nametable.Objects.Add(temp_eb3);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Element.LayoutMaster";
    static global::Uno.UX.Selector __selector1 = "indicator";
    static global::Uno.UX.Selector __selector2 = "page1Tab";
    static global::Uno.UX.Selector __selector3 = "page2Tab";
    static global::Uno.UX.Selector __selector4 = "page3Tab";
    static global::Uno.UX.Selector __selector5 = "page4Tab";
}
