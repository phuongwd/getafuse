[Uno.Compiler.UxGenerated]
public partial class HomePage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly HomePage __parent;
        [Uno.WeakReference] internal readonly HomePage __parentInstance;
        public Template(HomePage parent, HomePage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb18;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "temp_eb18"
        };
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Grid();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("title");
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp2 = new global::Fuse.Reactive.Data("getAll");
            var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp4 = new global::Fuse.Controls.Button();
            var temp_eb18 = new global::Fuse.Reactive.EventBinding(temp2, __g_nametable);
            __self.ColumnCount = 2;
            __self.Margin = float4(10f, 20f, 0f, 0f);
            temp.Bindings.Add(temp3);
            temp4.Text = "View All";
            temp4.Alignment = Fuse.Elements.Alignment.Right;
            temp4.Padding = float4(0f, 0f, 10f, 0f);
            global::Fuse.Gestures.Clicked.AddHandler(temp4, temp_eb18.OnEvent);
            temp4.Bindings.Add(temp_eb18);
            __g_nametable.Objects.Add(temp_eb18);
            __self.Children.Add(temp);
            __self.Children.Add(temp4);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly HomePage __parent;
        [Uno.WeakReference] internal readonly HomePage __parentInstance;
        public Template1(HomePage parent, HomePage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::SummaryItem();
            return __self;
        }
    }
    global::Uno.UX.Property<Uno.UX.Size> loading_Width_inst;
    global::Uno.UX.Property<Uno.UX.Size> loading_Height_inst;
    global::Uno.UX.Property<float> loading_Opacity_inst;
    global::Uno.UX.Property<Fuse.Elements.Visibility> loading_Visibility_inst;
    global::Uno.UX.Property<float> panelData_Opacity_inst;
    global::Uno.UX.Property<float> headerPanel_Opacity_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<Uno.UX.Size> temp1_Value_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<object> temp3_Items_inst;
    global::Uno.UX.Property<float> temp4_To_inst;
    global::Uno.UX.Property<float> overlayPanel_Opacity_inst;
    global::Uno.UX.Property<float4> topText_Color_inst;
    global::Uno.UX.Property<Fuse.Platform.StatusBarStyle> statusBarConfig_Style_inst;
    internal global::Fuse.iOS.StatusBarConfig statusBarConfig;
    internal global::Fuse.Controls.MultiLayoutPanel headerPanel;
    internal global::Fuse.Controls.StackPanel textPanel;
    internal global::TitleText topText;
    internal global::Fuse.Controls.Panel backgroundPicture;
    internal global::Fuse.Controls.Panel overlayPanel;
    internal global::Fuse.Controls.Panel loading;
    internal global::Fuse.Controls.StackPanel panelData;
    internal global::Fuse.Reactive.EventBinding temp_eb19;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "statusBarConfig",
        "headerPanel",
        "textPanel",
        "topText",
        "backgroundPicture",
        "overlayPanel",
        "loading",
        "panelData",
        "temp_eb19"
    };
    static HomePage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public HomePage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp5 = new global::Fuse.Reactive.This();
        loading = new global::Fuse.Controls.Panel();
        loading_Width_inst = new getAFuse_FuseElementsElement_Width_Property(loading, __selector0);
        var temp6 = new global::Fuse.Elements.WidthFunction(temp5);
        var temp7 = new global::Fuse.Reactive.This();
        loading_Height_inst = new getAFuse_FuseElementsElement_Height_Property(loading, __selector1);
        var temp8 = new global::Fuse.Elements.HeightFunction(temp7);
        var temp9 = new global::Fuse.Reactive.This();
        loading_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(loading, __selector2);
        loading_Visibility_inst = new getAFuse_FuseElementsElement_Visibility_Property(loading, __selector3);
        panelData = new global::Fuse.Controls.StackPanel();
        panelData_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(panelData, __selector2);
        headerPanel = new global::Fuse.Controls.MultiLayoutPanel();
        headerPanel_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(headerPanel, __selector2);
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp, __selector4);
        var temp10 = new global::Fuse.Reactive.Data("showOverlay");
        var temp1 = new global::Fuse.Animations.Change<Uno.UX.Size>(loading_Height_inst);
        temp1_Value_inst = new getAFuse_FuseAnimationsChangeUnoUXSize_Value_Property(temp1, __selector4);
        var temp11 = new global::Fuse.Elements.HeightFunction(temp9);
        var temp2 = new global::Fuse.Triggers.WhileFalse();
        temp2_Value_inst = new getAFuse_FuseTriggersWhileBool_Value_Property(temp2, __selector4);
        var temp12 = new global::Fuse.Reactive.Data("showOverlay");
        var temp3 = new global::Fuse.Reactive.Each();
        temp3_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp3, __selector5);
        var temp13 = new global::Fuse.Reactive.Data("dataSource");
        var temp14 = new global::Fuse.Reactive.This();
        var temp4 = new global::Fuse.Triggers.ScrollingAnimation();
        temp4_To_inst = new getAFuse_FuseTriggersScrollingAnimation_To_Property(temp4, __selector6);
        var temp15 = new global::Fuse.Elements.HeightFunction(temp14);
        overlayPanel = new global::Fuse.Controls.Panel();
        overlayPanel_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(overlayPanel, __selector2);
        topText = new global::TitleText();
        topText_Color_inst = new getAFuse_FuseControlsTextControl_Color_Property(topText, __selector7);
        statusBarConfig = new global::Fuse.iOS.StatusBarConfig();
        statusBarConfig_Style_inst = new getAFuse_FuseiOSStatusBarConfig_Style_Property(statusBarConfig, __selector8);
        var temp16 = new global::Fuse.Reactive.Data("pageOnActivated");
        var temp17 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        textPanel = new global::Fuse.Controls.StackPanel();
        var temp18 = new global::Fuse.Controls.StatusBarBackground();
        backgroundPicture = new global::Fuse.Controls.Panel();
        var temp19 = new global::Fuse.Controls.Image();
        var temp20 = new global::Fuse.Effects.Blur();
        var temp21 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp22 = new global::Fuse.Controls.Text();
        var temp23 = new global::Fuse.Reactive.DataBinding(loading_Width_inst, temp6, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp24 = new global::Fuse.Reactive.DataBinding(loading_Height_inst, temp8, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp25 = new global::Fuse.Controls.ScrollView();
        var temp26 = new global::Fuse.Motion.ScrollViewMotion();
        var temp27 = new global::Fuse.Controls.StackPanel();
        var temp28 = new global::Fuse.Animations.Change<float>(loading_Opacity_inst);
        var temp29 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(loading_Visibility_inst);
        var temp30 = new global::Fuse.Animations.Change<float>(panelData_Opacity_inst);
        var temp31 = new global::Fuse.Animations.Change<float>(headerPanel_Opacity_inst);
        var temp32 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp10, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp33 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp11, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp34 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(loading_Visibility_inst);
        var temp35 = new global::Fuse.Animations.Change<Uno.UX.Size>(loading_Height_inst);
        var temp36 = new global::Fuse.Animations.Change<float>(panelData_Opacity_inst);
        var temp37 = new global::Fuse.Animations.Change<float>(headerPanel_Opacity_inst);
        var temp38 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp12, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp39 = new Template(this, this);
        var temp40 = new Template1(this, this);
        var temp41 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp13, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp42 = new global::Fuse.Animations.Move();
        var temp43 = new global::Fuse.Reactive.DataBinding(temp4_To_inst, temp15, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp44 = new global::Fuse.Triggers.ScrollingAnimation();
        var temp45 = new global::Fuse.Animations.Move();
        var temp46 = new global::Fuse.Animations.Move();
        var temp47 = new global::Fuse.Animations.Scale();
        var temp48 = new global::Fuse.Animations.Change<float>(overlayPanel_Opacity_inst);
        var temp49 = new global::Fuse.Animations.Change<float4>(topText_Color_inst);
        var temp50 = new global::Fuse.Animations.Change<Fuse.Platform.StatusBarStyle>(statusBarConfig_Style_inst);
        var temp51 = new global::Fuse.Triggers.ScrollingAnimation();
        var temp52 = new global::Fuse.Animations.Scale();
        var temp53 = new global::Fuse.Navigation.Activated();
        var temp54 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb19 = new global::Fuse.Reactive.EventBinding(temp16, __g_nametable);
        temp17.Code = "\n        var Observable = require(\"FuseJS/Observable\");\n        var data = Observable();\n        var showOverlay = Observable(true);\n        var initData = false;\n\n        var tokens = localStorage.getItem('gaTokens');\n        tokens = JSON.parse(tokens);\n        \n        function pageOnActivated() {\n            if(initData) return;\n            var myRequest = new Request('https://sta.getabstract.com/api/v7/mobile/mobile-startpage', {\n                method: 'GET', \n                headers: {\n                    \"Authorization\": \"Bearer \" + tokens.access_token,\n                    'Accept': 'application/json, text/javascript, */*; q=0.01',\n                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',\n                    \"Device-Id\": \"CC3AC87A-C4B5-4806-97EF-34534543\",\n                    \"Application\":  \"APP_PHONE\",\n                    \"OS\": \"IOS\",\n                    \"Version\":  \"7.9.0\"\n                }\n            });\n            showOverlay.value = true;\n            fetch(myRequest).then(function(response) {\n                status = response.status;  // Get the HTTP status code\n                response_ok = response.ok; // Is response.status in the 200-range?\n\n                return response.json();    // This returns a promise\n            }).then(function(responseObject) {\n                \n                if(responseObject.error) {\n                    router.push('login');\n                } else {\n                    showOverlay.value = false;\n                    initData = true;\n                    data.replaceAll(responseObject.list);\n                }\n                // Do something with the result\n            }).catch(function(err) {\n                console.log('errrorrr');\n                conosole.log(err);\n                // An error occurred somewhere in the Promise chain\n            });\n        }\n\n        function getById(id) {\n            var params = '?list_id=' + id + '&page=0&max_numbers=100';  \n            var myRequest = new Request('https://sta.getabstract.com/api/v7/mobile/mobile-startpage-detail' + params, {\n                method: 'GET', \n                headers: {\n                    \"Authorization\": \"Bearer \" + tokens.access_token,\n                    'Accept': 'application/json, text/javascript, */*; q=0.01',\n                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',\n                    \"Device-Id\": \"CC3AC87A-C4B5-4806-97EF-34534543\",\n                    \"Application\":  \"APP_PHONE\",\n                    \"OS\": \"IOS\",\n                    \"Version\":  \"7.9.0\"\n                },\n            });\n            fetch(myRequest).then(function(response) {\n                status = response.status;  // Get the HTTP status code\n                response_ok = response.ok; // Is response.status in the 200-range?\n                // console.log('response');\n                // console.log(JSON.stringify(response));\n\n                return response.json();    // This returns a promise\n            }).then(function(responseObject) {\n                if(responseObject.error) {\n\n                } else {\n                    // console.log(JSON.stringify(responseObject));\n                }\n                // Do something with the result\n            }).catch(function(err) {\n                console.log('errrorrr');\n                conosole.log(err);\n                // An error occurred somewhere in the Promise chain\n            });\n        }\n\n        function getAll(sender) {\n            getById(sender.data.list_id);\n        }\n\n        function logOut() {\n            localStorage.removeItem(\"gaTokens\");\n            router.push('login');\n        }\n\n        function openDetail(sender) {\n            console.log(JSON.stringify(sender.data.title));\n        }\n\n        module.exports = {\n            dataSource: data,\n            logOut: logOut,\n            pageOnActivated: pageOnActivated,\n            getById: getById,\n            getAll: getAll,\n            openDetail: openDetail,\n            showOverlay: showOverlay\n        };\n    ";
        temp17.LineNumber = 3;
        temp17.FileName = "Pages/Home/HomePage.ux";
        statusBarConfig.Style = Fuse.Platform.StatusBarStyle.Light;
        statusBarConfig.Name = __selector9;
        headerPanel.HitTestMode = Fuse.Elements.HitTestMode.None;
        headerPanel.Height = new Uno.UX.Size(260f, Uno.UX.Unit.Unspecified);
        headerPanel.Alignment = Fuse.Elements.Alignment.Top;
        headerPanel.Opacity = 0f;
        headerPanel.Name = __selector10;
        headerPanel.Children.Add(textPanel);
        headerPanel.Children.Add(backgroundPicture);
        textPanel.Alignment = Fuse.Elements.Alignment.Center;
        textPanel.Name = __selector11;
        textPanel.Children.Add(temp18);
        textPanel.Children.Add(topText);
        temp18.Margin = float4(0f, 0f, 0f, 15f);
        topText.Value = "Today";
        topText.FontSize = 48f;
        topText.Margin = float4(5f, 5f, 5f, 5f);
        topText.Name = __selector12;
        backgroundPicture.Name = __selector13;
        backgroundPicture.Background = Fuse.Drawing.Brushes.Black;
        backgroundPicture.Children.Add(overlayPanel);
        backgroundPicture.Children.Add(temp19);
        overlayPanel.Color = float4(1f, 1f, 1f, 1f);
        overlayPanel.Opacity = 0.2f;
        overlayPanel.Name = __selector14;
        temp19.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp19.Alignment = Fuse.Elements.Alignment.Center;
        temp19.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/welcNutshell.jpg"));
        temp19.MemoryPolicy = Fuse.Resources.MemoryPolicy.UnloadUnused;
        temp19.Background = temp21;
        temp19.Children.Add(temp20);
        temp20.Radius = 1f;
        loading.Color = float4(1f, 1f, 1f, 1f);
        loading.Alignment = Fuse.Elements.Alignment.Left;
        loading.Visibility = Fuse.Elements.Visibility.Hidden;
        loading.Padding = float4(10f, 10f, 10f, 10f);
        loading.Opacity = 0f;
        loading.Layer = Fuse.Layer.Overlay;
        loading.Name = __selector15;
        loading.Children.Add(temp22);
        loading.Bindings.Add(temp23);
        loading.Bindings.Add(temp24);
        temp22.Value = "Loading...";
        temp22.Alignment = Fuse.Elements.Alignment.Center;
        temp25.ClipToBounds = true;
        temp25.Motion = temp26;
        temp25.Children.Add(temp27);
        temp25.Children.Add(temp4);
        temp25.Children.Add(temp44);
        temp25.Children.Add(temp51);
        temp25.Children.Add(temp53);
        temp26.Overflow = Fuse.Motion.OverflowType.Elastic;
        temp27.Alignment = Fuse.Elements.Alignment.Left;
        temp27.Padding = float4(0f, 260f, 0f, 0f);
        temp27.Children.Add(temp);
        temp27.Children.Add(temp2);
        temp27.Children.Add(panelData);
        temp.Animators.Add(temp1);
        temp.Animators.Add(temp28);
        temp.Animators.Add(temp29);
        temp.Animators.Add(temp30);
        temp.Animators.Add(temp31);
        temp.Bindings.Add(temp32);
        temp.Bindings.Add(temp33);
        temp1.Duration = 0.6;
        temp28.Value = 1f;
        temp28.Duration = 0.6;
        temp29.Value = Fuse.Elements.Visibility.Visible;
        temp30.Value = 0f;
        temp30.Duration = 0.6;
        temp31.Value = 0f;
        temp31.Duration = 0.6;
        temp2.Animators.Add(temp34);
        temp2.Animators.Add(temp35);
        temp2.Animators.Add(temp36);
        temp2.Animators.Add(temp37);
        temp2.Bindings.Add(temp38);
        temp34.Value = Fuse.Elements.Visibility.Hidden;
        temp35.Value = new Uno.UX.Size(0f, Uno.UX.Unit.Unspecified);
        temp35.Duration = 0.4;
        temp36.Value = 1f;
        temp36.Duration = 0.6;
        temp37.Value = 1f;
        temp37.Duration = 0.6;
        panelData.Color = float4(1f, 1f, 1f, 1f);
        panelData.Opacity = 0f;
        panelData.Name = __selector16;
        panelData.Children.Add(temp3);
        temp3.Templates.Add(temp39);
        temp3.Templates.Add(temp40);
        temp3.Bindings.Add(temp41);
        temp4.From = 0f;
        temp4.Animators.Add(temp42);
        temp4.Bindings.Add(temp43);
        temp42.Y = -1f;
        temp42.Duration = 1;
        temp42.Target = headerPanel;
        temp44.From = 0f;
        temp44.To = 260f;
        temp44.Animators.Add(temp45);
        temp44.Animators.Add(temp46);
        temp44.Animators.Add(temp47);
        temp44.Animators.Add(temp48);
        temp44.Animators.Add(temp49);
        temp44.Animators.Add(temp50);
        temp45.Y = -0.75f;
        temp45.RelativeTo = Fuse.TranslationModes.Size;
        temp45.Target = backgroundPicture;
        temp45.Easing = Fuse.Animations.Easing.QuadraticOut;
        temp46.Y = -0.38f;
        temp46.RelativeTo = Fuse.TranslationModes.ParentSize;
        temp46.Target = textPanel;
        temp46.Easing = Fuse.Animations.Easing.QuadraticOut;
        temp47.Factor = 0.5f;
        temp47.Target = textPanel;
        temp48.Value = 0.8f;
        temp49.Value = float4(0f, 0f, 0f, 1f);
        temp50.Value = Fuse.Platform.StatusBarStyle.Dark;
        temp51.From = 0f;
        temp51.To = -1000f;
        temp51.Animators.Add(temp52);
        temp52.Factor = 4f;
        temp52.Target = headerPanel;
        temp52.Easing = Fuse.Animations.Easing.Linear;
        temp53.Actions.Add(temp54);
        temp53.Bindings.Add(temp_eb19);
        temp54.Handler += temp_eb19.OnEvent;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(statusBarConfig);
        __g_nametable.Objects.Add(headerPanel);
        __g_nametable.Objects.Add(textPanel);
        __g_nametable.Objects.Add(topText);
        __g_nametable.Objects.Add(backgroundPicture);
        __g_nametable.Objects.Add(overlayPanel);
        __g_nametable.Objects.Add(loading);
        __g_nametable.Objects.Add(panelData);
        __g_nametable.Objects.Add(temp_eb19);
        this.Children.Add(temp17);
        this.Children.Add(statusBarConfig);
        this.Children.Add(headerPanel);
        this.Children.Add(loading);
        this.Children.Add(temp25);
    }
    static global::Uno.UX.Selector __selector0 = "Width";
    static global::Uno.UX.Selector __selector1 = "Height";
    static global::Uno.UX.Selector __selector2 = "Opacity";
    static global::Uno.UX.Selector __selector3 = "Visibility";
    static global::Uno.UX.Selector __selector4 = "Value";
    static global::Uno.UX.Selector __selector5 = "Items";
    static global::Uno.UX.Selector __selector6 = "To";
    static global::Uno.UX.Selector __selector7 = "Color";
    static global::Uno.UX.Selector __selector8 = "Style";
    static global::Uno.UX.Selector __selector9 = "statusBarConfig";
    static global::Uno.UX.Selector __selector10 = "headerPanel";
    static global::Uno.UX.Selector __selector11 = "textPanel";
    static global::Uno.UX.Selector __selector12 = "topText";
    static global::Uno.UX.Selector __selector13 = "backgroundPicture";
    static global::Uno.UX.Selector __selector14 = "overlayPanel";
    static global::Uno.UX.Selector __selector15 = "loading";
    static global::Uno.UX.Selector __selector16 = "panelData";
}
