[Uno.Compiler.UxGenerated]
public partial class MenuButton: MenuItem
{
    string _field_Text;
    [global::Uno.UX.UXOriginSetter("SetText")]
    public string Text
    {
        get { return _field_Text; }
        set { SetText(value, null); }
    }
    public void SetText(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Text)
        {
            _field_Text = value;
            OnPropertyChanged("Text", origin);
        }
    }
    global::Uno.UX.Property<string> label_Value_inst;
    global::Uno.UX.Property<string> this_Text_inst;
    internal global::Fuse.Controls.Text label;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "label"
    };
    static MenuButton()
    {
    }
    [global::Uno.UX.UXConstructor]
    public MenuButton()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Reactive.This();
        label = new global::Fuse.Controls.Text();
        label_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(label, __selector0);
        var temp1 = new global::Fuse.Reactive.Property(temp, getAFuse_accessor_MenuButton_Text.Singleton);
        this_Text_inst = new getAFuse_MenuButton_Text_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp2 = new global::Fuse.Reactive.DataBinding(label_Value_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Read);
        var temp3 = new global::Fuse.Gestures.WhilePressed();
        var temp4 = new global::Fuse.Animations.Scale();
        this.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        this.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        label.FontSize = 28f;
        label.Color = float4(0.9254902f, 0.9372549f, 0.945098f, 1f);
        label.Alignment = Fuse.Elements.Alignment.Top;
        label.Name = __selector2;
        label.Bindings.Add(temp2);
        temp3.Animators.Add(temp4);
        temp4.Factor = 0.85f;
        temp4.Duration = 0.1;
        temp4.Target = label;
        __g_nametable.This = this;
        __g_nametable.Objects.Add(label);
        __g_nametable.Properties.Add(this_Text_inst);
        this.Children.Add(label);
        this.Children.Add(temp3);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Text";
    static global::Uno.UX.Selector __selector2 = "label";
}
