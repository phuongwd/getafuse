[Uno.Compiler.UxGenerated]
public partial class ColorCircleSelected: Fuse.Controls.Circle
{
    static ColorCircleSelected()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ColorCircleSelected()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Circle();
        var temp1 = new global::Fuse.Drawing.Stroke();
        var temp2 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        this.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        this.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(13f, 13f, 13f, 13f);
        temp.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp.Height = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Strokes.Add(temp1);
        temp1.Width = 2f;
        temp1.Brush = temp2;
        this.Children.Add(temp);
    }
}
