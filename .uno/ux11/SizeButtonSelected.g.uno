[Uno.Compiler.UxGenerated]
public partial class SizeButtonSelected: Fuse.Controls.Circle
{
    string _field_Label;
    [global::Uno.UX.UXOriginSetter("SetLabel")]
    public string Label
    {
        get { return _field_Label; }
        set { SetLabel(value, null); }
    }
    public void SetLabel(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Label)
        {
            _field_Label = value;
            OnPropertyChanged("Label", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> this_Label_inst;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
    };
    static SizeButtonSelected()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SizeButtonSelected()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.This();
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, getAFuse_accessor_SizeButtonSelected_Label.Singleton);
        this_Label_inst = new getAFuse_SizeButtonSelected_Label_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Read);
        var temp4 = new global::Fuse.Drawing.SolidColor();
        var temp5 = new global::Fuse.Drawing.Stroke();
        var temp6 = new global::Fuse.Drawing.SolidColor();
        this.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        this.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(15f, 15f, 15f, 15f);
        temp.Color = float4(1f, 1f, 1f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Font = global::RotatingPages.Hind;
        temp.Bindings.Add(temp3);
        temp4.Color = float4(0.3333333f, 0.4313726f, 0.4784314f, 1f);
        temp5.Width = 2f;
        temp5.Brush = temp6;
        temp6.Color = float4(0.3333333f, 0.4313726f, 0.4784314f, 1f);
        __g_nametable.This = this;
        __g_nametable.Properties.Add(this_Label_inst);
        this.Fills.Add(temp4);
        this.Strokes.Add(temp5);
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Label";
}
