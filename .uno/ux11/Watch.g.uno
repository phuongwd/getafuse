[Uno.Compiler.UxGenerated]
public partial class Watch: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Watch __parent;
        [Uno.WeakReference] internal readonly Watch __parentInstance;
        public Template(Watch parent, Watch parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::FadeText();
            __self.Value = "RESET";
            __self.FontSize = 12f;
            __self.Color = float4(1f, 1f, 1f, 1f);
            global::Fuse.Controls.Grid.SetRow(__self, 1);
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Watch __parent;
        [Uno.WeakReference] internal readonly Watch __parentInstance;
        public Template1(Watch parent, Watch parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::FadeText();
            __self.Value = "LAP";
            __self.FontSize = 12f;
            __self.Color = float4(1f, 1f, 1f, 1f);
            global::Fuse.Controls.Grid.SetRow(__self, 1);
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template2: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Watch __parent;
        [Uno.WeakReference] internal readonly Watch __parentInstance;
        public Template2(Watch parent, Watch parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template2()
        {
        }
        public override object New()
        {
            var __self = new global::FadeText();
            __self.Value = "START";
            __self.Color = float4(0.6f, 0.6f, 0.6f, 1f);
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template3: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Watch __parent;
        [Uno.WeakReference] internal readonly Watch __parentInstance;
        public Template3(Watch parent, Watch parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template3()
        {
        }
        public override object New()
        {
            var __self = new global::FadeText();
            __self.Value = "STOP";
            __self.Color = float4(0.6f, 0.6f, 0.6f, 1f);
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template4: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Watch __parent;
        [Uno.WeakReference] internal readonly Watch __parentInstance;
        public Template4(Watch parent, Watch parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<string> temp1_Value_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb30;
        global::Uno.UX.NameTable __g_nametable;
        static string[] __g_static_nametable = new string[] {
            "temp_eb30"
        };
        static Template4()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp = new global::FadeText();
            temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp2 = new global::Fuse.Reactive.Data("title");
            __g_nametable = new global::Uno.UX.NameTable(__parent.__g_nametable, __g_static_nametable);
            var temp1 = new global::FadeText();
            temp1_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp1, __selector0);
            var temp3 = new global::Fuse.Reactive.Data("time");
            var temp4 = new global::Fuse.Reactive.Data("removeLap");
            var temp5 = new global::Fuse.Controls.DockPanel();
            var temp6 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp7 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp3, __g_nametable, Fuse.Reactive.BindingMode.Default);
            var temp8 = new global::Fuse.Controls.Rectangle();
            var temp9 = new global::Fuse.Triggers.AddingAnimation();
            var temp10 = new global::Fuse.Animations.Move();
            var temp11 = new global::Fuse.Triggers.RemovingAnimation();
            var temp12 = new global::Fuse.Animations.Move();
            var temp13 = new global::Fuse.Triggers.LayoutAnimation();
            var temp14 = new global::Fuse.Animations.Move();
            var temp_eb30 = new global::Fuse.Reactive.EventBinding(temp4, __g_nametable);
            __self.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
            __self.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
            global::Fuse.Gestures.Clicked.AddHandler(__self, temp_eb30.OnEvent);
            temp5.Children.Add(temp);
            temp5.Children.Add(temp1);
            temp.FontSize = 24f;
            temp.Color = float4(1f, 1f, 1f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.Center;
            global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
            temp.Bindings.Add(temp6);
            temp1.FontSize = 24f;
            temp1.Color = float4(1f, 1f, 1f, 1f);
            temp1.Alignment = Fuse.Elements.Alignment.Center;
            global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Right);
            temp1.Bindings.Add(temp7);
            temp8.Color = float4(0.5607843f, 0.7490196f, 0.9098039f, 1f);
            temp8.Height = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
            temp8.Alignment = Fuse.Elements.Alignment.Bottom;
            temp9.Animators.Add(temp10);
            temp10.Y = -1f;
            temp10.Duration = 0.3;
            temp10.RelativeTo = Fuse.TranslationModes.Size;
            temp11.Animators.Add(temp12);
            temp12.X = 1.4f;
            temp12.Duration = 0.3;
            temp12.RelativeTo = Fuse.TranslationModes.ParentSize;
            temp13.Animators.Add(temp14);
            temp14.Y = 1f;
            temp14.Duration = 0.3;
            temp14.RelativeTo = Fuse.Triggers.LayoutTransition.PositionLayoutChange;
            temp14.Easing = Fuse.Animations.Easing.CircularInOut;
            __g_nametable.Objects.Add(temp_eb30);
            __self.Children.Add(temp5);
            __self.Children.Add(temp8);
            __self.Children.Add(temp9);
            __self.Children.Add(temp11);
            __self.Children.Add(temp13);
            __self.Bindings.Add(temp_eb30);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<object> temp1_Value_inst;
    global::Uno.UX.Property<object> temp2_Value_inst;
    global::Uno.UX.Property<object> temp3_Items_inst;
    [global::Uno.UX.UXGlobalResource("DroidSans")] public static readonly Fuse.Font DroidSans;
    [global::Uno.UX.UXGlobalResource("Stopwatch")] public static readonly Stopwatch Stopwatch;
    internal global::Fuse.Reactive.EventBinding temp_eb28;
    internal global::Fuse.Reactive.EventBinding temp_eb29;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb28",
        "temp_eb29"
    };
    static Watch()
    {
        DroidSans = new global::Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Fonts/DroidSans/DroidSansMono.ttf")));
        Stopwatch = new global::Stopwatch();
        global::Uno.UX.Resource.SetGlobalKey(DroidSans, "DroidSans");
        global::Uno.UX.Resource.SetGlobalKey(Stopwatch, "Stopwatch");
    }
    [global::Uno.UX.UXConstructor]
    public Watch(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::FadeText();
        temp_Value_inst = new getAFuse_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("timeString");
        var temp1 = new global::Fuse.Reactive.Match();
        temp1_Value_inst = new getAFuse_FuseReactiveMatch_Value_Property(temp1, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("running");
        var temp6 = new global::Fuse.Reactive.Data("addLapOrReset");
        var temp2 = new global::Fuse.Reactive.Match();
        temp2_Value_inst = new getAFuse_FuseReactiveMatch_Value_Property(temp2, __selector0);
        var temp7 = new global::Fuse.Reactive.Data("running");
        var temp8 = new global::Fuse.Reactive.Data("stopStart");
        var temp3 = new global::Fuse.Reactive.Each();
        temp3_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp3, __selector1);
        var temp9 = new global::Fuse.Reactive.Data("laps");
        var temp10 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp11 = new global::Fuse.Drawing.ImageFill();
        var temp12 = new global::Fuse.Controls.DockPanel();
        var temp13 = new global::Fuse.Controls.Panel();
        var temp14 = new global::Fuse.Drawing.LinearGradient();
        var temp15 = new global::Fuse.Drawing.GradientStop();
        var temp16 = new global::Fuse.Drawing.GradientStop();
        var temp17 = new global::Fuse.Controls.StackPanel();
        var temp18 = new global::Fuse.Controls.StatusBarBackground();
        var temp19 = new global::FadeText();
        var temp20 = new global::Fuse.Controls.ScrollView();
        var temp21 = new global::Fuse.Controls.StackPanel();
        var temp22 = new global::Fuse.Controls.StackPanel();
        var temp23 = new global::WatchFace();
        var temp24 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp4, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp25 = new global::Fuse.Controls.Panel();
        var temp26 = new global::Fuse.Controls.Panel();
        var temp27 = new global::Fuse.Controls.Grid();
        var temp28 = new global::Fuse.Controls.Panel();
        var temp29 = new global::Fuse.Controls.Image();
        var temp30 = new global::Fuse.Reactive.Case();
        var temp31 = new Template(this, this);
        var temp32 = new global::Fuse.Reactive.Case();
        var temp33 = new Template1(this, this);
        var temp34 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp5, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp35 = new global::Fuse.Gestures.Clicked();
        var temp36 = new global::Fuse.Animations.Scale();
        temp_eb28 = new global::Fuse.Reactive.EventBinding(temp6, __g_nametable);
        var temp37 = new global::Fuse.Controls.Button();
        var temp38 = new global::Fuse.Reactive.Case();
        var temp39 = new Template2(this, this);
        var temp40 = new global::Fuse.Reactive.Case();
        var temp41 = new Template3(this, this);
        var temp42 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp7, __g_nametable, Fuse.Reactive.BindingMode.Default);
        var temp43 = new global::Fuse.Controls.Rectangle();
        var temp44 = new global::Fuse.Gestures.Clicked();
        var temp45 = new global::Fuse.Animations.Scale();
        temp_eb29 = new global::Fuse.Reactive.EventBinding(temp8, __g_nametable);
        var temp46 = new global::Fuse.Controls.StackPanel();
        var temp47 = new Template4(this, this);
        var temp48 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp9, __g_nametable, Fuse.Reactive.BindingMode.Default);
        temp10.LineNumber = 4;
        temp10.FileName = "Pages/StopWatch/StopWatch.ux";
        temp10.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Pages/StopWatch/MainView.js"));
        temp11.WrapMode = Fuse.Drawing.WrapMode.ClampToEdge;
        temp11.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/background.png"));
        temp12.Children.Add(temp13);
        temp12.Children.Add(temp20);
        temp13.Height = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.DockPanel.SetDock(temp13, Fuse.Layouts.Dock.Top);
        temp13.Background = temp14;
        temp13.Children.Add(temp17);
        temp14.StartPoint = float2(0f, 0f);
        temp14.EndPoint = float2(1f, 0f);
        temp14.Stops.Add(temp15);
        temp14.Stops.Add(temp16);
        temp15.Offset = 0f;
        temp15.Color = float4(0.2431373f, 0.3019608f, 0.5254902f, 1f);
        temp16.Offset = 1f;
        temp16.Color = float4(0.2235294f, 0.3607843f, 0.5411765f, 1f);
        temp17.Alignment = Fuse.Elements.Alignment.Center;
        temp17.Children.Add(temp18);
        temp17.Children.Add(temp19);
        global::Fuse.Controls.DockPanel.SetDock(temp18, Fuse.Layouts.Dock.Top);
        temp19.Value = "STOPWATCH";
        temp19.Color = float4(0.4823529f, 0.6156863f, 0.9960784f, 1f);
        temp19.Alignment = Fuse.Elements.Alignment.Center;
        temp20.SnapMinTransform = false;
        global::Fuse.Controls.DockPanel.SetDock(temp20, Fuse.Layouts.Dock.Fill);
        temp20.Children.Add(temp21);
        temp21.Width = new Uno.UX.Size(500f, Uno.UX.Unit.Unspecified);
        temp21.Children.Add(temp22);
        temp21.Children.Add(temp46);
        temp22.Children.Add(temp23);
        temp22.Children.Add(temp37);
        temp23.Width = new Uno.UX.Size(250f, Uno.UX.Unit.Unspecified);
        temp23.Height = new Uno.UX.Size(250f, Uno.UX.Unit.Unspecified);
        temp23.Margin = float4(30f, 40f, 30f, 30f);
        temp23.Children.Add(temp);
        temp23.Children.Add(temp25);
        temp.FontSize = 44f;
        temp.Color = float4(1f, 1f, 1f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Font = global::Watch.DroidSans;
        temp.Bindings.Add(temp24);
        temp25.Margin = float4(30f, 30f, 30f, 30f);
        temp25.Children.Add(temp26);
        temp26.Alignment = Fuse.Elements.Alignment.Bottom;
        global::Fuse.Gestures.Clicked.AddHandler(temp26, temp_eb28.OnEvent);
        temp26.Children.Add(temp27);
        temp26.Children.Add(temp35);
        temp26.Bindings.Add(temp_eb28);
        temp27.Rows = "auto,25";
        temp27.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        temp27.Children.Add(temp28);
        temp27.Children.Add(temp1);
        temp28.Width = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        temp28.Height = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.Grid.SetRow(temp28, 0);
        temp28.Children.Add(temp29);
        temp29.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/Refresh.png"));
        temp1.Cases.Add(temp30);
        temp1.Cases.Add(temp32);
        temp1.Bindings.Add(temp34);
        temp30.Bool = false;
        temp30.Factories.Add(temp31);
        temp32.Bool = true;
        temp32.Factories.Add(temp33);
        temp35.Animators.Add(temp36);
        temp36.Factor = 0.8f;
        temp36.Duration = 0.05;
        temp36.DurationBack = 0.15;
        temp36.Easing = Fuse.Animations.Easing.CircularInOut;
        temp37.Width = new Uno.UX.Size(250f, Uno.UX.Unit.Unspecified);
        temp37.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        global::Fuse.Gestures.Clicked.AddHandler(temp37, temp_eb29.OnEvent);
        temp37.Children.Add(temp2);
        temp37.Children.Add(temp43);
        temp37.Children.Add(temp44);
        temp37.Bindings.Add(temp_eb29);
        temp2.Cases.Add(temp38);
        temp2.Cases.Add(temp40);
        temp2.Bindings.Add(temp42);
        temp38.Bool = false;
        temp38.Factories.Add(temp39);
        temp40.Bool = true;
        temp40.Factories.Add(temp41);
        temp43.CornerRadius = float4(30f, 30f, 30f, 30f);
        temp43.Color = float4(0f, 1f, 1f, 1f);
        temp43.Layer = Fuse.Layer.Background;
        temp44.Animators.Add(temp45);
        temp45.Factor = 0.9f;
        temp45.Duration = 0.06;
        temp45.Easing = Fuse.Animations.Easing.CircularInOut;
        temp46.Margin = float4(20f, 40f, 20f, 40f);
        temp46.Children.Add(temp3);
        temp3.Templates.Add(temp47);
        temp3.Bindings.Add(temp48);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb28);
        __g_nametable.Objects.Add(temp_eb29);
        this.Background = temp11;
        this.Children.Add(temp10);
        this.Children.Add(temp12);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Items";
}
