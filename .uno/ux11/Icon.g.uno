[Uno.Compiler.UxGenerated]
public partial class Icon: Fuse.Controls.Image
{
    static Icon()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Icon()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Color = float4(0.3333333f, 0.4313726f, 0.4784314f, 1f);
        this.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(15f, 15f, 15f, 15f);
    }
}
