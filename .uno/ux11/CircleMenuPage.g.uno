[Uno.Compiler.UxGenerated]
public partial class CircleMenuPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly CircleMenuPage __parent;
        [Uno.WeakReference] internal readonly CircleMenuPage __parentInstance;
        public Template(CircleMenuPage parent, CircleMenuPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::CalendarItem();
            __self.MinHeight = new Uno.UX.Size(70f, Uno.UX.Unit.Unspecified);
            return __self;
        }
    }
    global::Uno.UX.Property<float> itemsScrollView_Opacity_inst;
    global::Uno.UX.Property<object> temp_Items_inst;
    internal global::Fuse.Triggers.WhileFalse hideHeadlines;
    internal global::Fuse.Controls.Panel leftHeadline;
    internal global::Fuse.Controls.Panel rightHeadline;
    internal global::Fuse.Triggers.WhileTrue showItems;
    internal global::Fuse.Controls.ScrollView itemsScrollView;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "hideHeadlines",
        "leftHeadline",
        "rightHeadline",
        "showItems",
        "itemsScrollView"
    };
    static CircleMenuPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public CircleMenuPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        itemsScrollView = new global::Fuse.Controls.ScrollView();
        itemsScrollView_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(itemsScrollView, __selector0);
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new getAFuse_FuseReactiveEach_Items_Property(temp, __selector1);
        var temp1 = new global::Fuse.Reactive.Data("items");
        var temp2 = new global::Fuse.Controls.ClientPanel();
        var temp3 = new global::Fuse.Drawing.ImageFill();
        var temp4 = new global::Fuse.Controls.Panel();
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Navigation.Activated();
        var temp7 = new global::Fuse.Triggers.Actions.Toggle();
        var temp8 = new global::Fuse.Triggers.Actions.Toggle();
        var temp9 = new global::Fuse.Controls.Panel();
        var temp10 = new global::WobbleButton();
        var temp11 = new global::Fuse.Controls.Grid();
        var temp12 = new global::Fuse.Controls.Grid();
        var temp13 = new global::HamburgerIcon();
        var temp14 = new global::Fuse.Controls.Panel();
        var temp15 = new global::SmallAvatarIcon();
        var temp16 = new global::Fuse.Gestures.Clicked();
        var temp17 = new global::Fuse.Triggers.Actions.Toggle();
        var temp18 = new global::Fuse.Triggers.Actions.Toggle();
        var temp19 = new global::Fuse.Animations.Scale();
        var temp20 = new global::Fuse.Controls.Grid();
        hideHeadlines = new global::Fuse.Triggers.WhileFalse();
        var temp21 = new global::Fuse.Animations.Move();
        var temp22 = new global::Fuse.Animations.Move();
        leftHeadline = new global::Fuse.Controls.Panel();
        var temp23 = new global::Fuse.Controls.Grid();
        var temp24 = new global::DefaultText();
        var temp25 = new global::Fuse.Controls.Panel();
        var temp26 = new global::MutedText();
        rightHeadline = new global::Fuse.Controls.Panel();
        var temp27 = new global::Fuse.Controls.Grid();
        var temp28 = new global::SunnyIcon();
        var temp29 = new global::DefaultText();
        var temp30 = new global::Fuse.Controls.Panel();
        var temp31 = new global::MutedText();
        var temp32 = new global::Fuse.Controls.Panel();
        showItems = new global::Fuse.Triggers.WhileTrue();
        var temp33 = new global::Fuse.Triggers.RemovingAnimation();
        var temp34 = new global::Fuse.Animations.Change<float>(itemsScrollView_Opacity_inst);
        var temp35 = new global::Fuse.Controls.StackPanel();
        var temp36 = new Template(this, this);
        var temp37 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp1, __g_nametable, Fuse.Reactive.BindingMode.Default);
        temp2.Background = temp3;
        temp2.Children.Add(temp4);
        temp2.Children.Add(temp5);
        temp2.Children.Add(temp6);
        temp2.Children.Add(temp9);
        temp3.WrapMode = Fuse.Drawing.WrapMode.ClampToEdge;
        temp3.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp3.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/bg.jpg"));
        temp5.LineNumber = 8;
        temp5.FileName = "Pages/CircleMenu/CircleMenu.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Pages/CircleMenu/CircleMenu.js"));
        temp6.Actions.Add(temp7);
        temp6.Actions.Add(temp8);
        temp7.Target = showItems;
        temp8.Target = hideHeadlines;
        temp9.Children.Add(temp10);
        temp9.Children.Add(temp11);
        temp10.Width = new Uno.UX.Size(300f, Uno.UX.Unit.Unspecified);
        temp10.Height = new Uno.UX.Size(300f, Uno.UX.Unit.Unspecified);
        temp10.Alignment = Fuse.Elements.Alignment.BottomRight;
        temp10.Offset = new Uno.UX.Size2(new Uno.UX.Size(100f, Uno.UX.Unit.Unspecified), new Uno.UX.Size(100f, Uno.UX.Unit.Unspecified));
        temp11.Rows = "1.3*,2*,6*";
        temp11.Children.Add(temp12);
        temp11.Children.Add(temp20);
        temp11.Children.Add(temp32);
        temp12.Columns = "1*,1*";
        temp12.Padding = float4(20f, 20f, 20f, 20f);
        global::Fuse.Controls.DockPanel.SetDock(temp12, Fuse.Layouts.Dock.Top);
        temp12.Children.Add(temp13);
        temp12.Children.Add(temp14);
        temp13.Color = float4(1f, 1f, 1f, 0.6f);
        temp13.StretchMode = Fuse.Elements.StretchMode.Fill;
        temp13.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp13.Height = new Uno.UX.Size(17f, Uno.UX.Unit.Unspecified);
        temp13.Alignment = Fuse.Elements.Alignment.Left;
        temp14.Width = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        temp14.Height = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        temp14.Alignment = Fuse.Elements.Alignment.Right;
        temp14.Children.Add(temp15);
        temp14.Children.Add(temp16);
        temp15.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp15.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp16.Animators.Add(temp19);
        temp16.Actions.Add(temp17);
        temp16.Actions.Add(temp18);
        temp17.Target = showItems;
        temp18.Target = hideHeadlines;
        temp19.Factor = 0.9f;
        temp19.Duration = 0.05;
        temp19.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp20.Columns = "2*,1*";
        temp20.Height = new Uno.UX.Size(80f, Uno.UX.Unit.Unspecified);
        temp20.Alignment = Fuse.Elements.Alignment.Top;
        temp20.Children.Add(hideHeadlines);
        temp20.Children.Add(leftHeadline);
        temp20.Children.Add(rightHeadline);
        hideHeadlines.Bypass = Fuse.Triggers.TriggerBypassMode.Standard;
        hideHeadlines.Name = __selector2;
        hideHeadlines.Animators.Add(temp21);
        hideHeadlines.Animators.Add(temp22);
        temp21.X = -1.2f;
        temp21.Duration = 0.5;
        temp21.RelativeTo = Fuse.TranslationModes.Size;
        temp21.Target = leftHeadline;
        temp21.Easing = Fuse.Animations.Easing.CircularInOut;
        temp22.X = 1.2f;
        temp22.Duration = 0.5;
        temp22.RelativeTo = Fuse.TranslationModes.Size;
        temp22.Target = rightHeadline;
        temp22.Easing = Fuse.Animations.Easing.CircularInOut;
        leftHeadline.Padding = float4(30f, 0f, 0f, 0f);
        leftHeadline.Name = __selector3;
        leftHeadline.Children.Add(temp23);
        temp23.Rows = "3*,2*";
        temp23.Children.Add(temp24);
        temp23.Children.Add(temp25);
        temp24.Value = "Monday";
        temp24.FontSize = 40.3f;
        temp24.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp25.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp25.Alignment = Fuse.Elements.Alignment.Bottom;
        temp25.Children.Add(temp26);
        temp26.Value = "February 8, 2015";
        temp26.FontSize = 15f;
        temp26.Alignment = Fuse.Elements.Alignment.CenterLeft;
        rightHeadline.Padding = float4(0f, 0f, 30f, 0f);
        rightHeadline.Name = __selector4;
        rightHeadline.Children.Add(temp27);
        temp27.Rows = "3*,2*";
        temp27.Columns = "1*,1*";
        temp27.Children.Add(temp28);
        temp27.Children.Add(temp29);
        temp27.Children.Add(temp30);
        temp28.Width = new Uno.UX.Size(35f, Uno.UX.Unit.Unspecified);
        temp28.Height = new Uno.UX.Size(35f, Uno.UX.Unit.Unspecified);
        global::Fuse.Controls.Grid.SetRow(temp28, 0);
        global::Fuse.Controls.Grid.SetColumn(temp28, 0);
        temp29.Value = "58°";
        temp29.FontSize = 37f;
        temp29.Width = new Uno.UX.Size(60f, Uno.UX.Unit.Unspecified);
        temp29.Alignment = Fuse.Elements.Alignment.Center;
        temp30.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp30.Alignment = Fuse.Elements.Alignment.Bottom;
        global::Fuse.Controls.Grid.SetRow(temp30, 1);
        global::Fuse.Controls.Grid.SetColumn(temp30, 0);
        global::Fuse.Controls.Grid.SetColumnSpan(temp30, 2);
        temp30.Children.Add(temp31);
        temp31.Value = "San Fransisco";
        temp31.FontSize = 12f;
        temp31.Alignment = Fuse.Elements.Alignment.Center;
        temp32.Margin = float4(20f, 0f, 0f, 0f);
        temp32.Children.Add(showItems);
        showItems.Name = __selector5;
        showItems.Nodes.Add(itemsScrollView);
        itemsScrollView.Name = __selector6;
        itemsScrollView.Children.Add(temp33);
        itemsScrollView.Children.Add(temp35);
        temp33.Animators.Add(temp34);
        temp34.Value = 0f;
        temp34.Duration = 0.6;
        temp34.Easing = Fuse.Animations.Easing.ExponentialOut;
        temp35.Children.Add(temp);
        temp.Templates.Add(temp36);
        temp.Bindings.Add(temp37);
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(hideHeadlines);
        __g_nametable.Objects.Add(leftHeadline);
        __g_nametable.Objects.Add(rightHeadline);
        __g_nametable.Objects.Add(showItems);
        __g_nametable.Objects.Add(itemsScrollView);
        this.Children.Add(temp2);
    }
    static global::Uno.UX.Selector __selector0 = "Opacity";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "hideHeadlines";
    static global::Uno.UX.Selector __selector3 = "leftHeadline";
    static global::Uno.UX.Selector __selector4 = "rightHeadline";
    static global::Uno.UX.Selector __selector5 = "showItems";
    static global::Uno.UX.Selector __selector6 = "itemsScrollView";
}
