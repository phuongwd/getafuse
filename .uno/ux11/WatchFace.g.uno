[Uno.Compiler.UxGenerated]
public partial class WatchFace: Fuse.Controls.Panel
{
    global::Uno.UX.Property<Fuse.Elements.Visibility> tickCircle_Visibility_inst;
    global::Uno.UX.Property<float> tickCircle_Opacity_inst;
    global::Uno.UX.Property<float> circleScale_Factor_inst;
    global::Uno.UX.Property<float> clock_EndAngleDegrees_inst;
    global::Uno.UX.Property<float> trackerBall_Degrees_inst;
    internal global::Fuse.Rotation trackerBall;
    internal global::Fuse.Controls.Circle clock;
    internal global::Fuse.Controls.Circle tickCircle;
    internal global::Fuse.Scaling circleScale;
    internal global::Fuse.Triggers.Timeline tickCircleAnimation;
    static WatchFace()
    {
    }
    [global::Uno.UX.UXConstructor]
    public WatchFace()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        tickCircle = new global::Fuse.Controls.Circle();
        tickCircle_Visibility_inst = new getAFuse_FuseElementsElement_Visibility_Property(tickCircle, __selector0);
        tickCircle_Opacity_inst = new getAFuse_FuseElementsElement_Opacity_Property(tickCircle, __selector1);
        circleScale = new global::Fuse.Scaling();
        circleScale_Factor_inst = new getAFuse_FuseScaling_Factor_Property(circleScale, __selector2);
        clock = new global::Fuse.Controls.Circle();
        clock_EndAngleDegrees_inst = new getAFuse_FuseControlsEllipticalShape_EndAngleDegrees_Property(clock, __selector3);
        trackerBall = new global::Fuse.Rotation();
        trackerBall_Degrees_inst = new getAFuse_FuseRotation_Degrees_Property(trackerBall, __selector4);
        var temp = new global::Fuse.Controls.Circle();
        var temp1 = new global::Fuse.Translation();
        var temp2 = new global::Fuse.Rotation();
        var temp3 = new global::Fuse.Drawing.Stroke();
        var temp4 = new global::Fuse.Drawing.ImageFill();
        var temp5 = new global::Fuse.Controls.Circle();
        var temp6 = new global::Fuse.Drawing.Stroke();
        var temp7 = new global::Fuse.Drawing.StaticSolidColor(float4(0.2705882f, 0.3294118f, 0.5764706f, 1f));
        var temp8 = new global::Fuse.Drawing.Stroke();
        var temp9 = new global::Fuse.Drawing.ImageFill();
        tickCircleAnimation = new global::Fuse.Triggers.Timeline();
        var temp10 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(tickCircle_Visibility_inst);
        var temp11 = new global::Fuse.Animations.Change<float>(tickCircle_Opacity_inst);
        var temp12 = new global::Fuse.Animations.Change<float>(circleScale_Factor_inst);
        var temp13 = new global::Seconds();
        var temp14 = new global::Fuse.Animations.Change<float>(clock_EndAngleDegrees_inst);
        var temp15 = new global::Fuse.Animations.Change<float>(trackerBall_Degrees_inst);
        var temp16 = new global::Fuse.Triggers.Actions.Pulse();
        temp.Color = float4(1f, 1f, 1f, 1f);
        temp.Width = new Uno.UX.Size(10f, Uno.UX.Unit.Unspecified);
        temp.Height = new Uno.UX.Size(10f, Uno.UX.Unit.Unspecified);
        temp.Children.Add(trackerBall);
        temp.Children.Add(temp1);
        trackerBall.Degrees = 0f;
        trackerBall.Name = __selector5;
        temp1.Y = -0.487f;
        temp1.RelativeTo = Fuse.TranslationModes.ParentSize;
        clock.StartAngleDegrees = 0f;
        clock.EndAngleDegrees = 0f;
        clock.Name = __selector6;
        clock.Strokes.Add(temp3);
        clock.Children.Add(temp2);
        temp2.Degrees = -90f;
        temp3.Width = 6f;
        temp3.Offset = -1f;
        temp3.Alignment = Fuse.Drawing.StrokeAlignment.Inside;
        temp3.LineCap = Fuse.Drawing.LineCap.Round;
        temp3.Brush = temp4;
        temp4.WrapMode = Fuse.Drawing.WrapMode.ClampToEdge;
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/gradient.png"));
        temp5.Strokes.Add(temp6);
        temp6.Width = 6f;
        temp6.Offset = -1f;
        temp6.Alignment = Fuse.Drawing.StrokeAlignment.Inside;
        temp6.Brush = temp7;
        tickCircle.Visibility = Fuse.Elements.Visibility.Hidden;
        tickCircle.Name = __selector7;
        tickCircle.Strokes.Add(temp8);
        tickCircle.Children.Add(circleScale);
        temp8.Width = 6f;
        temp8.Offset = -1f;
        temp8.Alignment = Fuse.Drawing.StrokeAlignment.Inside;
        temp8.Brush = temp9;
        temp9.WrapMode = Fuse.Drawing.WrapMode.ClampToEdge;
        temp9.File = new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../../Assets/Images/gradient.png"));
        circleScale.Name = __selector8;
        tickCircleAnimation.Name = __selector9;
        tickCircleAnimation.Animators.Add(temp10);
        tickCircleAnimation.Animators.Add(temp11);
        tickCircleAnimation.Animators.Add(temp12);
        temp10.Value = Fuse.Elements.Visibility.Visible;
        temp11.Value = 0f;
        temp11.Duration = 0.5;
        temp11.DurationBack = 0;
        temp11.Easing = Fuse.Animations.Easing.QuadraticOut;
        temp12.Value = 1.3f;
        temp12.Duration = 0.5;
        temp12.DurationBack = 0;
        temp12.Easing = Fuse.Animations.Easing.QuadraticOut;
        temp13.Stopwatch = global::Watch.Stopwatch;
        temp13.Animators.Add(temp14);
        temp13.Animators.Add(temp15);
        temp13.Actions.Add(temp16);
        temp14.Value = 360f;
        temp14.Duration = 1;
        temp15.Value = 360f;
        temp15.Duration = 1;
        temp16.Target = tickCircleAnimation;
        this.Children.Add(temp);
        this.Children.Add(clock);
        this.Children.Add(temp5);
        this.Children.Add(tickCircle);
        this.Children.Add(tickCircleAnimation);
        this.Children.Add(temp13);
    }
    static global::Uno.UX.Selector __selector0 = "Visibility";
    static global::Uno.UX.Selector __selector1 = "Opacity";
    static global::Uno.UX.Selector __selector2 = "Factor";
    static global::Uno.UX.Selector __selector3 = "EndAngleDegrees";
    static global::Uno.UX.Selector __selector4 = "Degrees";
    static global::Uno.UX.Selector __selector5 = "trackerBall";
    static global::Uno.UX.Selector __selector6 = "clock";
    static global::Uno.UX.Selector __selector7 = "tickCircle";
    static global::Uno.UX.Selector __selector8 = "circleScale";
    static global::Uno.UX.Selector __selector9 = "tickCircleAnimation";
}
