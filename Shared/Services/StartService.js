function getStartPage() {
    var tokens = localStorage.getItem('gaTokens');
    var myRequest = new Request('https://sta.getabstract.com/api/v7/mobile/mobile-startpage', {
        method: 'GET', 
        headers: {
            "Authorization": "Bearer " + tokens.access_token,
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            "Device-Id": "CC3AC87A-C4B5-4806-97EF-34534543",
            "Application":  "APP_PHONE",
            "OS": "IOS",
            "Version":  "7.9.0"
        }
    });
    fetch(myRequest).then(function(response) {
        status = response.status;  // Get the HTTP status code
        response_ok = response.ok; // Is response.status in the 200-range?
        // console.log('response');
        // console.log(JSON.stringify(response));

        return response.json();    // This returns a promise
    }).then(function(responseObject) {
        if(responseObject.error) {
            router.push('login');
        } else {

            router.push('home', responseObject.list);
            // for (var i = responseObject.list.length - 1; i >= 0; i--) {
            //     getById(responseObject.list[i].list_id)
            // }

            // console.log(JSON.stringify(data));
        }
        // Do something with the result
    }).catch(function(err) {
        console.log('errrorrr');
        conosole.log(err);
        // An error occurred somewhere in the Promise chain
    });
}